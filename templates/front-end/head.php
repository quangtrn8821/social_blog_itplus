<!doctype html>
<html class="no-js" lang="en">


<!-- Mirrored from axilthemes.com/demo/template/layout/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Dec 2021 12:39:28 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cloud Travel</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->

    <!-- JQuery Validate -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="public/layout/assets/js/vendor/jquery.validate.min.js"></script>

    <!-- Swal 2 -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <link rel="stylesheet" href="public/layout/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="public/layout/assets/css/vendor/font-awesome.css">
    <link rel="stylesheet" href="public/layout/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="public/layout/assets/css/vendor/slick-theme.css">
    <link rel="stylesheet" href="public/layout/assets/css/vendor/base.css">
    <link rel="stylesheet" href="public/layout/assets/css/plugins/plugins.css">
    <link rel="stylesheet" href="public/layout/assets/css/style.css">
    <!-- Toast -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <style>
        .reply-comment-form {
            display: none;
        }
        .post-notification {
            border-radius: 5px;
            box-shadow: 0px 0px 2px #888;
            padding: 10px;
            padding-left: 20px;
        }
        .post-notification:hover {
            transition-duration: 0.8s;
            box-shadow: 0px 0px 2px 1px #888;
        }
        .login-container {
            display: flex;
            align-items: flex-start;
            margin-top: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>

</head>
<body>
<header>
    <div class="container">
        <?php
        include("templates/toast/toast.php");
        if (isset($error_1)) {
            $message_error = json_encode($error_1);
            echo "<script> toastError($message_error) </script>";
        }
        if (isset($success)) {
            $message_success = json_encode($success);
            echo "<script> toastSuccess($message_success) </script>";
        }
        ?>
    </div>
</header>