<!-- Start Footer Area  -->
<div class="axil-footer-area axil-footer-style-1 footer-variation-2">
    <!-- Start Footer Top Area  -->
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-md-4">
                    <div class="logo">
                        <a href="index.html">
                            <img class="dark-logo" src="public/layout/assets/images/logo/logo.jpg"
                                 alt="Logo Images">
                            <img class="white-logo" src="public/layout/assets/images/logo/logo-white2.png"
                                 alt="Logo Images">
                        </a>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8">
                    <!-- Start Post List  -->
                    <div class="d-flex justify-content-start mt_sm--15 justify-content-md-end align-items-center flex-wrap">
                        <h5 class="follow-title mb--0 mr--20">Follow Us</h5>
                        <ul class="social-icon color-tertiary md-size justify-content-start">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                    <!-- End Post List  -->
                </div>

            </div>
        </div>
    </div>
    <!-- End Footer Top Area  -->

    <!-- Start Copyright Area  -->
        <div class="copyright-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-md-8">
                        <div class="copyright-left">
                            <ul class="mainmenu justify-content-start">
                                <li>
                                    <a class="hover-flip-item-wrapper" href="#">
                                                <span class="hover-flip-item">
                                            <span data-text="Đào Gia Bảo">Đào Gia Bảo</span>
                                                </span>
                                    </a>
                                </li>
                                <li>
                                    <a class="hover-flip-item-wrapper" href="#">
                                                <span class="hover-flip-item">
                                            <span data-text="Nguyễn Thanh Nam">Nguyễn Thanh Nam</span>
                                                </span>
                                    </a>
                                </li>
                                <li>
                                    <a class="hover-flip-item-wrapper" href="#">
                                                <span class="hover-flip-item">
                                            <span data-text="Trần Văn Quang">Trần Văn Quang</span>
                                                </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="copyright-right text-start text-md-end mt_sm--20">
                            <p class="b3">Đồ án © 2022</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- End Copyright Area  -->
</div>
<!-- End Footer Area  -->

<!-- Start Back To Top  -->
<a id="backto-top"></a>
<!-- End Back To Top  -->

</div>

<script src="public/layout/assets/js/vendor/modernizr.min.js"></script>
<!-- jQuery JS -->
<script src="public/layout/assets/js/vendor/jquery.js"></script>
<!-- Bootstrap JS -->
<script src="public/layout/assets/js/vendor/bootstrap.min.js"></script>
<script src="public/layout/assets/js/vendor/slick.min.js"></script>
<script src="public/layout/assets/js/vendor/tweenmax.min.js"></script>
<script src="public/layout/assets/js/vendor/js.cookie.js"></script>
<script src="public/layout/assets/js/vendor/jquery.style.switcher.js"></script>


<!-- Main JS -->
<script src="public/layout/assets/js/main.js"></script>


<!--link jquery to user ajax-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<!--script pusher-->
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = false;

    const pusher = new Pusher('7505f78179f81f44562f', {
        cluster: 'ap1'
    });

    const channelComment = pusher.subscribe('comment-channel')
    channelComment.bind('add-comment', (data) => {
        //apend data here
        $.post('views/item/item_comment.php', data,
            (result) => {
                $('#comment-list').append(result)
            })
    });

    const channelReplyComment = pusher.subscribe('reply-comment-channel')
    channelReplyComment.bind('add-reply-comment', (data) => {
        $.post('views/item/item_reply_comment.php', data,
            (result) => {
                id_binh_luan = '#comment-' + data.id_binh_luan
                $(id_binh_luan).append(result)
            }
        )
    })
    const session_email = "<?php echo $_SESSION["user"]; ?>"
    const notificationChannel = pusher.subscribe('notification-channel')
    notificationChannel.bind('add-comment', (data) => {
        if(data.email_tac_gia == session_email) {
            toastSuccess(data.ten_nguoi_dung + " đã bình luận về bài viết của bạn");
        }
        if(window.location.href.includes("social_blog_itplus/notification.php") && data.email_tac_gia == session_email) {
            location.reload()
        }
    })

    notificationChannel.bind('add-reply-comment', (data) => {
        console.log(data.email_tac_gia, session_email)
        if(data.email_tac_gia == session_email) {
            toastSuccess(data.ten_nguoi_dung + " đã trả lời bình luận của bạn");
        }
        if(window.location.href.includes("social_blog_itplus/notification.php") && data.email_tac_gia == session_email) {
            location.reload()
        }
    })
</script>

</body>

</html>