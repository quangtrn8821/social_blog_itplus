<?php
if (isset($_SESSION["user"])) {
    $display_sign_in = "none";
    $display_sign_out = "block";
    $display_avatar = "block";
    $display_notification = "block";
    foreach ($user as $index => $value) {
        $image = $value->anh_dai_dien;
    }
} else {
    $display_sign_in = "block";
    $display_sign_out = "none";
    $display_avatar = "none";
    $display_notification = "none";
}
?>
<body>
<div class="main-wrapper">
    <div class="mouse-cursor cursor-outer"></div>
    <div class="mouse-cursor cursor-inner"></div>
    <!-- Start Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <table class="table table-borderless align-middle">
                        <tr>
                            <td rowspan="4"><img style="margin: auto; display: block" class="dark-logo"
                                                 src="public/layout/assets/images/logo/logo.jpg"
                                                 alt="Logo Images"></td>
                            <td style="text-align: center"><h5>Tham gia cộng đồng của chúng tôi</h5></td>
                        </tr>
                        <tr>
                            <td><a href="sign_in.php">
                                    <button type="submit" style="width: 70%; margin: auto; display: block"
                                            class="axil-button button-rounded center">Đăng nhập
                                    </button>
                                </a></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; font-size: small">hoặc</td>
                        </tr>
                        <tr>
                            <td><a href="sign_up.php">
                                    <button type="submit" style="width: 70%; margin: auto; display: block"
                                            class="axil-button button-rounded color-success center">Tạo tài khoản mới
                                    </button>
                                </a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->
    <!-- Start Header -->
    <header class="header axil-header  header-light header-sticky ">
        <div class="header-wrap">
            <div class="row justify-content-between align-items-center">
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-3 col-12">
                    <div class="logo">
                        <a href="index.php">
                            <img class="dark-logo" src="public/layout/assets/images/logo/logo.jpg"
                                 alt="Blogar logo">
                        </a>
                    </div>
                </div>

                <div class="col-xl-6 d-none d-xl-block">
                    <div class="mainmenu-wrapper">
                        <nav class="mainmenu-nav">
                            <!-- Start Mainmanu Nav -->
                            <ul class="mainmenu">
                                <li><a href="index.php" class="active">Trang chủ</a></li>
                                <li><a href="post_list.php">Bài viết</a></li>
                                <li class="menu-item-has-children"><a href="#">Địa điểm</a>
                                    <ul class="axil-submenu">
                                        <li>
                                            <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=1">
                                                    <span class="hover-flip-item">
                                                    <span data-text="Sapa">Sapa</span>
                                                    </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=2">
                                                    <span class="hover-flip-item">
                                                    <span data-text="Đà Nẵng">Đà Nẵng</span>
                                                    </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=3">
                                                    <span class="hover-flip-item">
                                                    <span data-text="Phú Quốc">Phú Quốc</span>
                                                    </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=4">
                                                    <span class="hover-flip-item">
                                                    <span data-text="Nha Trang">Nha Trang</span>
                                                    </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li style="display:<?php echo $display_notification ?>"><a href="notification.php">Thông báo</a></li>
                            </ul>
                            <!-- End Mainmanu Nav -->
                        </nav>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-8 col-md-8 col-sm-9 col-12">
                    <div class="header-search text-end d-flex align-items-center">
                        <form class="header-search-form d-sm-block d-none" method="POST" action="post_list.php">
                            <div class="axil-search form-group">
                                <button type="submit" class="search-button" name="search-btn" value="search-btn"><i
                                            class="fal fa-search"></i></button>
                                <input type="text" class="form-control" placeholder="Tìm kiếm" name="ten_bai_viet">
                            </div>
                        </form>
                        <div class="mobile-search-wrapper d-sm-none d-block">
                            <button class="search-button-toggle"><i class="fal fa-search"></i></button>
                            <form class="header-search-form">
                                <div class="axil-search form-group">
                                    <button type="submit" class="search-button"><i class="fal fa-search"></i></button>
                                    <input type="text" class="form-control" placeholder="Search">
                                </div>
                            </form>
                        </div>
                        <ul class="metabar-block">
                            <li style="display: <?php echo $display_avatar; ?>">
                                <a href="user.php">
                                    <img src="public/image/user/<?php echo $image; ?>" style="width: 40px; height:40px;" alt="Author Images">
                                </a>
                            </li>
                            <li class="icon" style="display: <?php echo $display_sign_out; ?>"><a href="sign_out.php"><i class="fas fa-sign-out"></i></a></li>
                            <li class="icon" style="display: <?php echo $display_sign_in; ?>"><a href="sign_in.php"><i class="fas fa-sign-in"></i></a></li>
                        </ul>
                        <!-- Start Hamburger Menu  -->
                        <div class="hamburger-menu d-block d-xl-none">
                            <div class="hamburger-inner">
                                <div class="icon"><i class="fal fa-bars"></i></div>
                            </div>
                        </div>
                        <!-- End Hamburger Menu  -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Start Header -->

    <!-- Start Mobile Menu Area  -->
    <div class="popup-mobilemenu-area">
        <div class="inner">
            <div class="mobile-menu-top">
                <div class="logo">
                    <a href="index.html">
                        <img class="dark-logo" src="public/layout/assets/images/logo/logo.jpg" alt="Logo Images">
                        <img class="light-logo" src="public/layout/assets/images/logo/logo-white2.png"
                             alt="Logo Images">
                    </a>
                </div>
                <div class="mobile-close">
                    <div class="icon">
                        <i class="fal fa-times"></i>
                    </div>
                </div>
            </div>
            <ul class="mainmenu">
                <li><a href="index.php">Tiêu điểm</a></li>
                <li><a href="post_list.php">Bài viết</a></li>
                <li><a href="#">Thể loại</a></li>
            </ul>
        </div>
    </div>
    <!-- End Mobile Menu Area  -->