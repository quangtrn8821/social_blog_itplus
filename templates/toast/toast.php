<script src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
<script>
    function toastError(message) {
        Toastify({
            text: message,
            className: 'info',
            style: {
                background: '#e74c3c',
            },
            position: 'center',
        }).showToast();
    }

    function toastSuccess(message) {
        Toastify({
            text: message,
            className: 'info',
            style: {
                background: '#2ecc71',
            },
            position: 'center',
        }).showToast();
    }

    function toastErrorComment(message) {
        Toastify({
            text: message,
            className: 'error',
            gravity: 'top',
            position: 'right',
            style: {
                background: '#e74c3c',
            },
        }).showToast();
    }

    function toastSuccessComment(message) {
        Toastify({
            text: message,
            className: 'info',
            gravity: 'top',
            position: 'right',
            style: {
                background: '#2ecc71',
            },
        }).showToast();
    }

    function toastInfo(message) {
        Toastify({
            text: message,
            className: 'info',
            style: {
                background: '#0d6efd',
            },
        }).showToast();
    }
</script>
