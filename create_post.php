<?php
session_start();
include "controllers/c_create_post.php";
$c_create_post =  new c_create_post();

if(!empty($_POST)) {
    $c_create_post -> add_post();
    die();
}

if (isset($_SESSION["user"])) {
    $c_create_post -> index();
} else {
    header("location: sign_in.php");
}

?>