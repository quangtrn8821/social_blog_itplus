<?php
session_start();
include "controllers/c_edit_post.php";
$c_edit_post =  new c_edit_post();

if(isset($_POST["submit"])) {
    $c_edit_post -> edit_post();
}

if (isset($_SESSION["user"])) {
    $c_edit_post -> index();
} else {
    header("location: sign_in.php");
}

?>