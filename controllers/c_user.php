<?php
include("models/user/m_user.php");
include("models/post/m_post_list.php");

class c_user
{
    public function __construct()
    {
    }

    public function get_user()
    {
        $m_user = new m_user();
        $user = $m_user->read_user_by_email($_SESSION["user"]);
        $m_post_list = new m_post_list();
        $user_post_list = $m_post_list->read_user_post_list($_SESSION["user"]);
        if ($_SESSION["role"] == "admin") {
            $all_post_list = $m_post_list->read_all_post_list();
        }
        $view = "views/user/v_user.php";
        include "./templates/front-end/layout.php";
    }

    public function logout()
    {
        unset($_SESSION["user"]);
        unset($_SESSION["role"]);
        session_destroy();
        header("location: sign_in.php");
    }

    public function edit_user()
    {
        $id = "";
        $image = "";
        $allowed = array('png', 'jpg');
        $m_user = new m_user();
        $user = $m_user->read_user_by_email($_SESSION["user"]);
        foreach ($user as $key => $value) {
            $id = $value->id;
            $image = $value->anh_dai_dien;
        }
        // update user
        if (isset($_POST["submit"])) {
            $ten = $_POST["ten"];
            $email = $_POST["email"];
            $sdt = $_POST["sdt"];
            $mo_ta = $_POST["mo_ta"];
            $anh_dai_dien = ($_FILES["anh_dai_dien"]["error"] == 0) ? (time() . "_" . $_FILES["anh_dai_dien"]["name"]) : $image;
            // Validate
            if (empty($ten)) {
                $error_1 = 'Họ tên không được bỏ trống';
            }
            if (empty($email)) {
                $error_1 = 'Email không được bỏ trống';
            }
            if (empty($sdt)) {
                $error_1 = 'Số điện thoại không được bỏ trống';
            }
            $extension = pathinfo($anh_dai_dien, PATHINFO_EXTENSION);
            if (!in_array($extension, $allowed)) {
                $error_1 = 'Chỉ chấp nhận dạng file png hoặc jgp';
            }
            if (!isset($error_1)) {
                $result = $m_user->update_user($id, $ten, $email, $sdt, $mo_ta, $anh_dai_dien);
                if ($result) {
                    $_SESSION["user"] = $email;
                    if ($anh_dai_dien != "") {
                        move_uploaded_file($_FILES["anh_dai_dien"]["tmp_name"], "public/image/user/" . $anh_dai_dien);
                        $success = "Cập nhật thành công";
                    } else {
                        $error_1 = "Cập nhật thất bại";
                    }
                }
            }
        }
        $view = "views/user/v_edit_user.php";
        include "./templates/front-end/layout.php";
    }

    public function delete_post($id_bai_viet)
    {
        $m_post_list = new m_post_list();
        $user = $m_post_list->read_user_by_id_post($id_bai_viet);
        $email = $user[0]->email;

        if ($_SESSION["role"] === "user" && $_SESSION["user"] === $email) {
            $result = $m_post_list->delete_post($id_bai_viet);
            if ($result) {
                $success = "Xóa thành công";
            } else {
                $error_1 = "Xóa thất bại";
            }
        }

        if ($_SESSION["role"] == "admin") {
            $result = $m_post_list->delete_post($id_bai_viet);
            if ($result) {
                $success = "Xóa thành công";
            } else {
                $error_1 = "Xóa thất bại";
            }
        }
    }
}
