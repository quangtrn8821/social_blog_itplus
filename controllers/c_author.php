<?php
include("models/user/m_user.php");
include("models/post/m_post_list.php");

class c_author
{
    public function __construct()
    {
    }

    public function get_author()
    {
        if (isset($_SESSION["user"])) {
            $m_user = new m_user();
            $user = $m_user->read_user_by_email($_SESSION["user"]);
        }

        $id_bai_viet = $_GET["id_bai_viet"];
        $m_post_list = new m_post_list();
        $author = $m_post_list->read_user_by_id_post($id_bai_viet);
        $email_author = $author[0]->email;
        $name_author = $author[0]->ten;
        $user_post_list = $m_post_list->read_user_post_list($email_author);
        $view = "views/author/v_author.php";
        include "./templates/front-end/layout.php";
    }
}

?>