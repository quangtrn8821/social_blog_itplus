<?php
include("models/user/m_sign_in.php");

class c_sign_in
{
    public function __construct()
    {
    }

    public function index()
    {
        $view = "views/sign_in/v_sign_in.php";
        include "./templates/front-end/layout_sign_in.php";
    }

    public function signin()
    {
        $email = $_POST["email"];
        $mat_khau = $_POST["password"];
        $m_sign_in = new m_sign_in();
        $user = $m_sign_in->read_user_by_id_pass($email, $mat_khau);

        if (empty($user)) {
            $error_1 = "Bạn đã nhập sai tài khoản hoặc mật khẩu";
        } else {
            foreach ($user as $key=>$value) {
                $email = $value->email;
                $role = $value->phan_quyen;
            }
            session_start();
            header( "location: user.php" );
            $_SESSION["user"] = $email;
            $_SESSION["role"] = $role;
        }
        $view = "views/sign_in/v_sign_in.php";
        include "./templates/front-end/layout_sign_in.php";
    }
}

?>
