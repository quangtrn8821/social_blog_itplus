<?php
include ("models/user/m_user.php");

class c_home_default {
    public function __construct()
    {
    }

    public function index() {
        if (isset($_SESSION["user"])) {
            $m_user = new m_user();
            $user = $m_user->read_user_by_email($_SESSION["user"]);
        }
        include "models/home/m_home.php";
        $m_home = new m_home();
        $technology = $m_home ->showPostTechnology();
        $technology2 = $m_home ->showPostTechnology2();
        $culinary = $m_home ->showPostCulinary();
        $culinary2 = $m_home ->showPostCulinary2();
        $travel = $m_home -> showPostTravel();
        $travel2 = $m_home -> showPostTravel2();
        $fashion = $m_home ->showPostFashion();
        $fashion2 = $m_home ->showPostFashion2();
        $fashion3 = $m_home ->showPostFashion3();
        $view = "views/home/v_home.php";
        include "./templates/front-end/layout.php";
    }
}


?>