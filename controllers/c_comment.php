<?php
include("models/comment/m_comment.php");
include_once("models/user/m_user.php");
include_once("models/notification/m_notification.php");
include_once("models/post/m_post_list.php");
date_default_timezone_set('Asia/Ho_Chi_Minh');

class c_comment
{
    public function __construct()
    {
    }

    //get comment
    public function index()
    {
        $id_bai_viet = $_GET['id_bai_viet'];

        $m_comment = new m_comment();
        $comment = $m_comment->get_comment($id_bai_viet);

        include_once("views/post/v_post_comment.php");
    }


    public function add_comment()
    {
        $m_comment = new m_comment();

        if(empty($_SESSION["user"])) {
            echo "You must logged in";
            die();
        }

        $m_user = new m_user();
        $user = $m_user->read_user_by_email($_SESSION["user"]);

        //get comment user data
        $id_nguoi_dung = $user[0]->id;
        $anh_dai_dien = $user[0] -> anh_dai_dien;
        $ten = $user[0] -> ten;
        $thoi_gian = date('Y-m-d H:i:s', time());

        if (isset($_POST['id_bai_viet']) && isset($_POST['noi_dung'])) {
            $id_bai_viet = $_POST['id_bai_viet'];
            $noi_dung = $_POST['noi_dung'];

            if (empty($_POST['noi_dung'])) {
                echo "No comment added";
                die();
            }

            $result = $m_comment->add_comment($noi_dung, $id_bai_viet, $id_nguoi_dung);
            $last_comment_id = $m_comment -> get_last_comment_id();
            $data = array_values(get_object_vars($last_comment_id[0]));
            $id_binh_luan = $data[0];

            //add notification
            $m_post_list = new m_post_list();
            $author = $m_post_list->read_user_by_id_post($_POST['id_bai_viet']);
            $id_tac_gia = $author[0]->id;
            $m_notification = new m_notification();
            $m_notification->add_notification("Đã bình luận bài viết của bạn", $id_bai_viet, $id_nguoi_dung, $id_tac_gia);

            //pusher send data to client
            $pusher_result = array("id_binh_luan" => $id_binh_luan,
                                    "noi_dung" => $noi_dung,
                                    "ten" => $ten,
                                    "anh" => $anh_dai_dien,
                                    "thoi_gian" => $thoi_gian);

            $pusher_notification_result = array("noi_dung" => "Đã bình luận bài viết của bạn",
                                                "id_bai_viet" => $id_bai_viet,
                                                "ten_nguoi_dung" => $user[0] -> ten,
                                                "email_tac_gia" =>  $author[0]->email,
                                                "ten_tac_gia" =>  $author[0]->ten,
                                                "thoi_gian" => $thoi_gian);

            include_once ("pusher/server.php");
            //trigger comment
            $pusher->trigger('comment-channel', 'add-comment', $pusher_result);

            $pusher->trigger('notification-channel', 'add-comment', $pusher_notification_result);

            if (!isset($result)) {
                echo "failed";
                die();
            }
            echo "success";
        }
    }
}

?>