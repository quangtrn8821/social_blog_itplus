<?php
include_once("models/post/m_edit_post.php");
include("models/user/m_user.php");

class c_edit_post
{
    public function __construct()
    {
    }

    public function index()
    {
        if (isset($_SESSION["user"])) {
            $m_user = new m_user();
            $user = $m_user->read_user_by_email($_SESSION["user"]);
        }
        if(isset($_POST["id_bai_viet"])) {
            $m_edit_post = new m_edit_post();
            $post = $m_edit_post->get_post_by_id_bai_viet($_POST["id_bai_viet"]);
        }
        $view = "views/post/v_edit_post.php";
        include "./templates/front-end/layout.php";
    }

    public function edit_post()
    {
        $id_bai_viet = $_POST['id_bai_viet'];
        $ten_bai_viet = $_POST['ten_bai_viet'];
        $noi_dung = $_POST['noi_dung'];

        $m_user = new m_user();
        $user = $m_user->read_user_by_email($_SESSION["user"]);
        $id_nguoi_dung = $user[0]->id;
        $m_edit_post = new m_edit_post();
        $result = $m_edit_post->update_post($ten_bai_viet, $noi_dung, $id_bai_viet);
        header("location: user.php");
    }
}
?>

