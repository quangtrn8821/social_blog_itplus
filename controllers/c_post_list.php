<?php
//include("models/user/m_user.php");
include ("./models/post/m_post_list.php");
date_default_timezone_set('Asia/Ho_Chi_Minh');

class c_post_list {
    public function __construct()
    {
    }

    public function index() {
        // get user

        if (isset($_SESSION["user"])) {
            $m_user = new m_user();
            $user = $m_user->read_user_by_email($_SESSION["user"]);
        }

        //call method show list
        $m_post_list =  new m_post_list();

        $post_list = $m_post_list -> showPostList();
        $page_number=$m_post_list->showPageNumber();

        //load view
        $view = "views/post/v_post_list.php";
        include "./templates/front-end/layout.php";
    }
}
?>