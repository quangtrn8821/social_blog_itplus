<?php
include "./models/post/m_post.php";

class c_post {

    public function __construct()
    {
    }

    public function index() {
        // get user
        include("models/user/m_user.php");
        if (isset($_SESSION["user"])) {
            $m_user = new m_user();
            $user = $m_user->read_user_by_email($_SESSION["user"]);
        }
        $m_post =  new m_post();
        $post = $m_post -> showPost();
        $view = "views/post/v_post_detail.php";
        //$comment = "views/post/v_post_comment.php";
        include "./templates/front-end/layout.php";
    }
}
?>