<?php
include("models/user/m_sign_up.php");

class c_sign_up
{
    public function __construct()
    {
    }

    public function index()
    {
        $view = "views/sign_up/v_sign_up.php";
        include "./templates/front-end/layout_sign_in.php";
    }

    public function add_user()
    {
        $m_sign_up = new m_sign_up();
        if (isset($_POST["sign_up"])) {
            $ten = $_POST["username"];
            $email = $_POST["email"];
            $sdt = $_POST["sdt"];
            $mat_khau = md5($_POST["password"]);
            $result_1 = $m_sign_up->return_user_by_email($email);
            // Validate
            if (empty($ten)) {
                $error_1 = 'Chưa nhập họ tên';
            }
            if (empty($email)) {
                $error_1 = 'Chưa nhập email';
            }
            if (empty($sdt)) {
                $error_1 = 'Chưa nhập số điện thoại';
            }
            if (empty($mat_khau)) {
                $error_1 = 'Chưa nhập mật khẩu';
            }
            if ($result_1->KQ == 1) {
                $error_1 = 'Email đã tồn tại';
            }
            if (!isset($error_1)) {
                $result = $m_sign_up->add_user($ten, $email, $sdt, $mat_khau);

                if ($result) {
                    $success = 'Đăng ký thành công';
                    header( "refresh:1;url=sign_in.php" );
                } else {
                    $error_1 = 'Đăng ký thất bại';
                }
            }
        }
        $view = "views/sign_up/v_sign_up.php";
        include "./templates/front-end/layout_sign_in.php";
    }
}
?>