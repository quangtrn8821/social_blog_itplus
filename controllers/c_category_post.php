<?php

class c_category_post {
    public function __construct()
    {
    }

    public function index() {
        // get user
        include("models/user/m_user.php");
        if (isset($_SESSION["user"])) {
            $m_user = new m_user();
            $user = $m_user->read_user_by_email($_SESSION["user"]);
        }
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        //call method show list
        include "./models/post/m_catergory_post.php";
        $m_catergory_post =  new m_catergory_post();
        $post_list = $m_catergory_post -> showPostList();
        $ten_the_loai = $post_list[0]->ten_the_loai;

        //load view
        $view = "views/post/v_catergory_post.php";
        include "./templates/front-end/layout.php";
    }
}
?>
