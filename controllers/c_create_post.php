<?php
include_once("models/post/m_create_post.php");
include("models/user/m_user.php");
date_default_timezone_set('Asia/Ho_Chi_Minh');

class c_create_post
{
    public function __construct()
    {
    }

    public function index()
    {
        if (isset($_SESSION["user"])) {
            $m_user = new m_user();
            $user = $m_user->read_user_by_email($_SESSION["user"]);
        }

        $view = "views/post/v_create_post.php";
        include "./templates/front-end/layout.php";
    }

    public function add_post()
    {

        $ten_bai_viet = $_POST['ten_bai_viet'];
        $the_loai = $_POST['the_loai'];
        $noi_dung = $_POST['noi_dung'];
        $anh_tieu_de = $_FILES["anh_tieu_de"]["name"];
        $thoi_gian_tao = date('Y-m-d H:i:s', time());

        $anh_1 = $_FILES["anh_1"]["name"];
        $anh_2 = $_FILES["anh_2"]["name"];

        $m_user = new m_user();
        $user = $m_user->read_user_by_email($_SESSION["user"]);
        $id_nguoi_dung = $user[0]->id;

        $m_create_post = new m_create_post();

        $result = $m_create_post->create_post($ten_bai_viet, $noi_dung, $the_loai, $anh_tieu_de, $id_nguoi_dung, $thoi_gian_tao);

        if ($result) {
            move_uploaded_file($_FILES["anh_tieu_de"]["tmp_name"], "public/image/post/" . $anh_tieu_de);

            if (isset($_FILES["anh_1"])) {
                move_uploaded_file($_FILES["anh_1"]["tmp_name"], "public/image/post/" . $anh_1);
            }

            if (isset($_FILES["anh_2"])) {
                move_uploaded_file($_FILES["anh_2"]["tmp_name"], "public/image/post/" . $anh_2);
            }
        }

        echo "<script>alert('Thêm bài viết thành công')</script>";

        header("location: post_list.php");
    }
}

?>
