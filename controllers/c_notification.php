<?php
include_once("models/notification/m_notification.php");
include_once("models/user/m_user.php");

class c_notification
{
    public function __construct()
    {
    }

    public function index()
    {
        $m_user = new m_user();
        $user = $m_user->read_user_by_email($_SESSION["user"]);
        $m_notification = new m_notification();
        $notification = $m_notification->get_notification($user[0]->id);

        $view = "views/notification/v_notification.php";
        include "./templates/front-end/layout.php";
    }
}

?>