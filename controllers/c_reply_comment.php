<?php
include_once "models/comment/m_reply_comment.php";
include_once "models/comment/m_comment.php";
include_once "models/user/m_user.php";
include_once("models/notification/m_notification.php");
include_once("models/post/m_post_list.php");
date_default_timezone_set('Asia/Ho_Chi_Minh');

class c_reply_comment
{
    public function __construct()
    {
    }

    //get reply comment
    public function index($id_binh_luan)
    {
        $m_reply_comment = new m_reply_comment();
        $reply_comment = $m_reply_comment -> get_reply_comment($id_binh_luan);

        include ("views/post/v_reply_comment.php");
    }

    public function add_reply_comment() {
        $m_reply_comment = new m_reply_comment();

        if(empty($_SESSION["user"])) {
            echo "You must logged in";
            die();
        }

        $m_user = new m_user();
        $user = $m_user->read_user_by_email($_SESSION["user"]);

        $id_nguoi_dung = $user[0]->id;
        $anh_dai_dien = $user[0] -> anh_dai_dien;
        $ten = $user[0] -> ten;
        $thoi_gian = date('Y-m-d H:i:s', time());

        if (isset($_POST['noi_dung']) && isset($_POST['id_binh_luan'])) {
            $noi_dung = $_POST['noi_dung'];
            $id_binh_luan = $_POST['id_binh_luan'];

            if (empty($_POST['noi_dung'])) {
                echo "No comment added";
                die();
            }

            $result = $m_reply_comment->add_reply_comment($noi_dung, $id_nguoi_dung, $id_binh_luan);

            //add notification
            $m_comment = new m_comment();
            $id = $m_comment->get_id_by_id_comment($id_binh_luan);
            $id_tac_gia = $id[0]->id_nguoi_dung;
            $id_bai_viet = $id[0]->id_bai_viet;
            $m_notification = new m_notification();
            $m_notification->add_notification("Đã trả lời bình luận của bạn", $id_bai_viet, $id_nguoi_dung, $id_tac_gia);

            $m_user = new m_user();
            $author = $m_user -> read_user_by_id($id_tac_gia);

            $pusher_result = array('id_binh_luan' => $id_binh_luan,
                                    'noi_dung' => $noi_dung,
                                    "ten" => $ten,
                                    "anh_dai_dien" => $anh_dai_dien,
                                    "thoi_gian" => $thoi_gian);

            $pusher_reply_comment = array("noi_dung" => "Đã trả lời bình luận của bạn",
                                           "thoi_gian" => $thoi_gian ,
                                            "ten_nguoi_dung" => $user[0] -> ten,
                                            "email_tac_gia" =>  $author[0] -> email,
                                            "ten_tac_gia" => $user[0] -> ten);

            include_once ("pusher/server.php");
            $pusher->trigger('reply-comment-channel', 'add-reply-comment', $pusher_result);

            $pusher->trigger('notification-channel', 'add-reply-comment', $pusher_reply_comment);

            if (!isset($result)) {
                echo "failed";
                die();
            }
            echo "success";
        }
    }
}

?>