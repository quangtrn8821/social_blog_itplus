-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 05, 2022 lúc 05:21 PM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `social_blog_itplus`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bai_viet`
--

CREATE TABLE `bai_viet` (
  `id` int(11) NOT NULL,
  `ten_bai_viet` varchar(255) NOT NULL,
  `noi_dung` text NOT NULL,
  `thoi_gian_tao` datetime NOT NULL,
  `id_the_loai` int(11) NOT NULL,
  `trang_thai` tinyint(2) NOT NULL DEFAULT 1,
  `anh_tieu_de` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `id_nguoi_dung` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `bai_viet`
--

INSERT INTO `bai_viet` (`id`, `ten_bai_viet`, `noi_dung`, `thoi_gian_tao`, `id_the_loai`, `trang_thai`, `anh_tieu_de`, `id_nguoi_dung`) VALUES
(20, 'Kinh nghiệm du lịch Sapa – Nơi gặp gỡ đất trời', '<h2>Sapa nơi gặp gỡ đất trời</h2>\r\nLặng lẽ Sapa\r\nNhắc đến Sapa người ta liên tưởng tới ngay “Lặng lẽ Sapa” – tác phẩm khiến Sapa hiện ra một cách ngọt ngào, lãng mạn luôn làm xao xuyến mong ước của tất cả các khách du lịch khi đặt chân đến miền lảng bảng sương buông, Một Sapa tươi thắm sắc hoa vào mùa xuân, khi hè gõ cửa là xanh ngát mát lành, nàng thơ e ấp với hương thu man mác và lãng mạn trong lớp áo tuyết trắng tinh khôi khi đông giá lạnh về. Nếu muốn ngắm muôn hoa đua nở, bạn hãy lựa chọn đi  <b>du lịch Sapa</b>  vào mùa xuân.\r\n<b>du lịch Sapa</b> vào mùa hạ nếu muốn nhìn thấy những ô màu ruộng bậc thang long lánh  vươn mình đón nắng. Thu sang – Sapa khoác lên mình một màu vàng óng của lúa chín. Mùa thu tại Sapa luôn thi vị, đong đầy cảm xúc và trở thành “ người tình” ngọt ngào đắm say của biết bao lữ khách.\r\n\r\nSapa khi đông về mang mác mong ngóng hơi ấm đan vào nhau. Một Sapa băng giá mà vẫn  nên thơ trong những ngày đông rét buốt. Du lịch Sapa bốn mùa, mùa nào cũng đẹp. Con người Sapa ai cũng dễ mến, dễ thương. Để ai đã từng qua đây đều không khỏi nhung nhớ về một phương trời Tây Bắc.\r\n<figure class=\"wp-block-image\"> <img src=\"public/image/post/cay-thong-co-don-sapa-manh-tien-khoi-600x600.jpg\"> </figure>\r\nCây thông cô đơn (Manh Tien Khoi)\r\n<h2>Sapa ở đâu?</h2>\r\nSapa là một thị trấn thuộc vùng cao huyện Sapa, thuộc tỉnh Lào Cai, ở phía Tây Bắc Việt Nam Việt Nam. Thị trấn Sa Pa nằm ở độ cao 1.600 mét so với mực nước biển. Sapa cách trung tâm thành phố Lào Cai 38 km và 376 km nếu tính từ Hà Nội. \r\n\r\nSapa là nơi sinh sống của 6 dân tộc anh em là: H’Mông, Kinh, Tày, Dao Đỏ, Giáy, Xã Phó. Các dân tộc ở Sapa đều có những lễ hội văn hóa mang nét đặc trưng riêng, như:\r\n\r\nHội sải sán (đạp núi) của người H’Mông.\r\nHội roóng pọc của người Giáy vào tháng Giêng âm lịch.\r\nLễ tết nhảy của người Dao diễn ra vào tháng Tết hàng năm.\r\n<h2>Thời tiết Sapa như thế nào?</h2>\r\nThời tiết Sapa thay đổi theo từng mùa trong năm, nhưng có lúc lại thay đổi theo từng ngày. Thời tiết Sapa đỏng đảnh, thất thường, được ví như nàng thiếu nữ lần đầu biết yêu. Sapa ở miền Bắc Việt Nam, khu vực khí hậu nhiệt đới ẩm gió mùa, nhưng do địa hình cao nên Sapa mang khí hậu cận nhiệt đới ẩm, ôn đới.\r\n<figure class=\"wp-block-image\"> <img src=\"public/image/post/tuyet-roi-o-sapa-hoangia-700x394.jpg\"> </figure>\r\nTuyết rơi ở Sapa (Hoang Gia)\r\n\r\nThời tiết Sapa tháng 7, 8, 9\r\nNhiệt độ trung bình tại Sapa vào tháng 8 khoảng 20 độ C, dao động từ 10 đến 28 độ C.\r\n\r\nThời tiết Sapa tháng 10\r\nThời điểm này ở Sapa, ban ngày thời tiết khá mát mẻ, ít mưa. Chiều tối về đêm, khi nhiệt độ giảm xuống, thời tiết sẽ hơi se se lạnh.\r\n\r\nThời tiết Sapa tháng 11\r\nTháng 11 lên Sapa để vương vấn chút thu còn xót lại trên thị trấn nơi núi cao. Trời bắt đầu chuyển lạnh, nắng nhạt dần và sương mù từng đợt theo gió bao phủ.\r\n\r\nThời tiết Sapa tháng 12, 1, 2\r\nĐây là 3 tháng mùa đông lạnh nhất trong năm, nhiệt độ thường xuyên dưới 10 độ C. Trong thời gian này thỉnh thoảng cũng có ngày nắng đẹp, nhưng đa phần có sương mù, thời tiết ẩm ướt.\r\n\r\nVào tháng 12, nhiệt độ giảm xuống rất thấp, có lúc, có nơi dưới 0 độ C. Vào khoảng thời gian này, Sapacó thể xuất hiện tuyết rơi. Tuyết rơi phổ biến nhất là từ ngày 15 tháng 12 đến ngày 10 tháng 1. \r\n\r\nThời tiết Sapa tháng 3, 4:\r\nTháng 3 trời bắt đầu đỡ lạnh hơn và tháng 4 trời bắt đầu ấm dần nên, mưa cũng hầu như không còn.\r\n\r\nThời tiết Sapa tháng 5, 6\r\nTrời nắng đẹp. Cả Sapa như giũ mình bật khỏi mùa đông lạnh giá cùng mùa xuân ẩm ướt để chào đón từng cơn mưa rào tầm tã. Mưa làm không khí tươi mát đồng thời cung cấp nước cho từng thửa ruộng bậc thang, người dân nơi đây lại tất bật vào mùa cấy, mùa của những cây mạ non nghiêng bóng dưới nắng hè chói chang.\r\n\r\nLên Sapa vào khoảng thời gian này, đừng quên thưởng thức trái đào, trái mận hậu Sapa, mận Bắc Hà.', '2022-12-05 23:09:36', 1, 1, 'dieu-gi-lam-nen-sapa-tuyet-voi-d.png', 1),
(21, '7 điều khách Tây hứng thú khi du lịch tại Việt Nam', '<p style=\"margin-left:0px; margin-right:0px; text-align:justify\"><strong><em>Dựa theo &yacute; kiến của c&aacute;c blogger du lịch tr&ecirc;n thế giới, trang Fiona Travelsfrom Asia đ&atilde; tổng hợp lại 7 trải nghiệm tại Việt Nam được kh&aacute;ch quốc tế y&ecirc;u th&iacute;ch.</em></strong></p>\r\n\r\n<h2 style=\"font-style:normal; margin-left:0px; margin-right:0px; text-align:justify\">7 điều kh&aacute;ch T&acirc;y hứng th&uacute; khi du lịch Việt Nam</h2>\r\n\r\n<div class=\"aligncenter wp-caption\" id=\"attachment_333616\" style=\"-webkit-text-stroke-width:0px; border:0px; clear:both; color:#111111; display:block; font-family:&quot;Open Sans&quot;,&quot;Helvetica Neue&quot;,Calibri,&quot;Droid Sans&quot;,Helvetica,Arial,sans-serif; font-size:14px; font-stretch:inherit; font-style:normal; font-variant-caps:normal; font-variant-east-asian:inherit; font-variant-ligatures:normal; font-variant-numeric:inherit; font-weight:400; letter-spacing:normal; line-height:inherit; margin-bottom:0.4em; margin-left:1.25em; margin-right:1.25em; margin-top:0.4em; max-width:96%; orphans:2; padding:0px; text-align:center; text-decoration-color:initial; text-decoration-style:initial; text-decoration-thickness:initial; text-indent:0px; text-transform:none; vertical-align:baseline; white-space:normal; widows:2; width:1034px; word-spacing:0px\"><img alt=\"Leo núi ở Sa Pa (Lào Cai) không phải là hoạt động du lịch phổ biến với người Việt nhưng lại hấp dẫn các du khách nước ngoài. Blogger Ella, chủ trang du lịch Many More Maps, từng đến Việt Nam và ấn tượng với Sa Pa. Nữ blogger chia sẻ thị trấn sương mù là địa điểm lý tưởng để rời xa thành phố ồn ào. Trong chuyến khám phá Sa Pa, Ella đã trekking dọc theo thung lũng Mường Hoa đến bản Y Linh Hồ và Lao Chải. Cô cảm thấy thú vị khi tìm hiểu về văn hóa bản địa của dân tộc H\'Mông sinh sống tại đây. Ảnh: tullie._.x.\" class=\"size-full wp-image-333616\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-1.jpg\" style=\"border:0px; display:block; font:inherit; height:auto; margin:0px auto; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" title=\"Leo núi ở Sa Pa (Lào Cai) không phải là hoạt động du lịch phổ biến với người Việt nhưng lại hấp dẫn các du khách nước ngoài. Blogger Ella, chủ trang du lịch Many More Maps, từng đến Việt Nam và ấn tượng với Sa Pa. Nữ blogger chia sẻ thị trấn sương mù là địa điểm lý tưởng để rời xa thành phố ồn ào. Trong chuyến khám phá Sa Pa, Ella đã trekking dọc theo thung lũng Mường Hoa đến bản Y Linh Hồ và Lao Chải. Cô cảm thấy thú vị khi tìm hiểu về văn hóa bản địa của dân tộc H\'Mông sinh sống tại đây. Ảnh: tullie._.x.\" />\r\n<p style=\"margin-left:0px; margin-right:0px\">Leo n&uacute;i ở Sa Pa (L&agrave;o Cai) kh&ocirc;ng phải l&agrave; hoạt động du lịch phổ biến với người Việt nhưng lại hấp dẫn c&aacute;c du kh&aacute;ch nước ngo&agrave;i. Blogger Ella, chủ trang du lịch Many More Maps, từng đến Việt Nam v&agrave; ấn tượng với Sa Pa. Nữ blogger chia sẻ thị trấn sương m&ugrave; l&agrave; địa điểm l&yacute; tưởng để rời xa th&agrave;nh phố ồn &agrave;o. Trong chuyến kh&aacute;m ph&aacute; Sa Pa, Ella đ&atilde; trekking dọc theo thung lũng Mường Hoa đến bản Y Linh Hồ v&agrave; Lao Chải. C&ocirc; cảm thấy th&uacute; vị khi t&igrave;m hiểu về văn h&oacute;a bản địa của d&acirc;n tộc H&rsquo;M&ocirc;ng sinh sống tại đ&acirc;y. Ảnh: tullie._.x.</p>\r\n</div>\r\n\r\n<p style=\"margin-left:0px; margin-right:0px; text-align:center\"><img alt=\"7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-2\" class=\"aligncenter size-full wp-image-333617\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-2.jpg\" style=\"border:0px; clear:both; display:block; font:inherit; height:auto; margin:0.4em auto 1.25em; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" /></p>\r\n\r\n<div style=\"-webkit-text-stroke-width:0px; border:0px; color:#111111; font-family:&quot;Open Sans&quot;,&quot;Helvetica Neue&quot;,Calibri,&quot;Droid Sans&quot;,Helvetica,Arial,sans-serif; font-size:14px; font-stretch:inherit; font-style:normal; font-variant-caps:normal; font-variant-east-asian:inherit; font-variant-ligatures:normal; font-variant-numeric:inherit; font-weight:400; height:344px; letter-spacing:normal; line-height:inherit; margin-bottom:15px; margin-left:0px; margin-right:0px; margin-top:15px; orphans:2; padding:0px; text-align:start; text-decoration-color:initial; text-decoration-style:initial; text-decoration-thickness:initial; text-indent:0px; text-transform:none; vertical-align:baseline; white-space:normal; widows:2; word-spacing:0px\"><iframe height=\"319\" scrolling=\"no\" src=\"https://www.ivivu.com/hothotel/?placeslug=viet-nam&amp;number=1&amp;width=481\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\" width=\"100%\"></iframe></div>\r\n\r\n<div class=\"aligncenter wp-caption\" id=\"attachment_333618\" style=\"-webkit-text-stroke-width:0px; border:0px; clear:both; color:#111111; display:block; font-family:&quot;Open Sans&quot;,&quot;Helvetica Neue&quot;,Calibri,&quot;Droid Sans&quot;,Helvetica,Arial,sans-serif; font-size:14px; font-stretch:inherit; font-style:normal; font-variant-caps:normal; font-variant-east-asian:inherit; font-variant-ligatures:normal; font-variant-numeric:inherit; font-weight:400; letter-spacing:normal; line-height:inherit; margin-bottom:0.4em; margin-left:1.25em; margin-right:1.25em; margin-top:0.4em; max-width:96%; orphans:2; padding:0px; text-align:center; text-decoration-color:initial; text-decoration-style:initial; text-decoration-thickness:initial; text-indent:0px; text-transform:none; vertical-align:baseline; white-space:normal; widows:2; width:1034px; word-spacing:0px\"><img alt=\"Ngoài các bản làng dân tộc thiểu số, Fansipan cũng là diểm đến mê hoặc nhiều du khách. Trang tư vấn du lịch Tayaramuse gợi ý du khách nên chinh phục đỉnh núi trong chuyến thăm Việt Nam. Nếu không có kinh nghiệm leo núi, bạn có thể chinh phục nóc nhà Đông Nam Á bằng cáp treo. Chỉ trong 10 phút di chuyển, du khách sẽ được ngắm trọn thị trấn mờ sương từ trên cao và đặt chân đến đỉnh Fansipan hùng vĩ. Ảnh: Tramemm, phuongdunq.\" class=\"size-full wp-image-333618\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-3.jpg\" style=\"border:0px; display:block; font:inherit; height:auto; margin:0px auto; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" title=\"Ngoài các bản làng dân tộc thiểu số, Fansipan cũng là diểm đến mê hoặc nhiều du khách. Trang tư vấn du lịch Tayaramuse gợi ý du khách nên chinh phục đỉnh núi trong chuyến thăm Việt Nam. Nếu không có kinh nghiệm leo núi, bạn có thể chinh phục nóc nhà Đông Nam Á bằng cáp treo. Chỉ trong 10 phút di chuyển, du khách sẽ được ngắm trọn thị trấn mờ sương từ trên cao và đặt chân đến đỉnh Fansipan hùng vĩ. Ảnh: Tramemm, phuongdunq.\" />\r\n<p style=\"margin-left:0px; margin-right:0px\">Ngo&agrave;i c&aacute;c bản l&agrave;ng d&acirc;n tộc thiểu số, Fansipan cũng l&agrave; diểm đến m&ecirc; hoặc nhiều du kh&aacute;ch. Trang tư vấn du lịch Tayaramuse gợi &yacute; du kh&aacute;ch n&ecirc;n chinh phục đỉnh n&uacute;i trong chuyến thăm Việt Nam. Nếu kh&ocirc;ng c&oacute; kinh nghiệm leo n&uacute;i, bạn c&oacute; thể chinh phục n&oacute;c nh&agrave; Đ&ocirc;ng Nam &Aacute; bằng c&aacute;p treo. Chỉ trong 10 ph&uacute;t di chuyển, du kh&aacute;ch sẽ được ngắm trọn thị trấn mờ sương từ tr&ecirc;n cao v&agrave; đặt ch&acirc;n đến đỉnh Fansipan h&ugrave;ng vĩ. Ảnh: Tramemm, phuongdunq.</p>\r\n</div>\r\n\r\n<div class=\"aligncenter wp-caption\" id=\"attachment_333619\" style=\"-webkit-text-stroke-width:0px; border:0px; clear:both; color:#111111; display:block; font-family:&quot;Open Sans&quot;,&quot;Helvetica Neue&quot;,Calibri,&quot;Droid Sans&quot;,Helvetica,Arial,sans-serif; font-size:14px; font-stretch:inherit; font-style:normal; font-variant-caps:normal; font-variant-east-asian:inherit; font-variant-ligatures:normal; font-variant-numeric:inherit; font-weight:400; letter-spacing:normal; line-height:inherit; margin-bottom:0.4em; margin-left:1.25em; margin-right:1.25em; margin-top:0.4em; max-width:96%; orphans:2; padding:0px; text-align:center; text-decoration-color:initial; text-decoration-style:initial; text-decoration-thickness:initial; text-indent:0px; text-transform:none; vertical-align:baseline; white-space:normal; widows:2; width:1034px; word-spacing:0px\"><img alt=\"Một địa điểm khác ở vùng núi phía bắc được nhắc đến là Lũng Cú (Hà Giang), điểm địa đầu đất nước. Blogger Fiona, chủ sở hữu trang Fiona travels from Asia, cho biết trải nghiệm đáng thử nhất khi đến Việt Nam là đặt chân đến điểm cực bắc Việt Nam. Fiona miêu tả về kỷ niệm chinh phục Lũng Cú: &quot;Nằm ở độ cao 1.470 m so với mực nước biển, cột cờ Lũng Cú nghiêm trang trong khung cảnh núi non hùng vĩ. Hành trình leo đến đỉnh Lũng Cú khá vất vả, nhưng trải nghiệm bạn nhận được rất xứng đáng&quot;. Ảnh: bi_tabu.\" class=\"size-full wp-image-333619\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-4.jpg\" style=\"border:0px; display:block; font:inherit; height:auto; margin:0px auto; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" title=\"Một địa điểm khác ở vùng núi phía bắc được nhắc đến là Lũng Cú (Hà Giang), điểm địa đầu đất nước. Blogger Fiona, chủ sở hữu trang Fiona travels from Asia, cho biết trải nghiệm đáng thử nhất khi đến Việt Nam là đặt chân đến điểm cực bắc Việt Nam. Fiona miêu tả về kỷ niệm chinh phục Lũng Cú: &quot;Nằm ở độ cao 1.470 m so với mực nước biển, cột cờ Lũng Cú nghiêm trang trong khung cảnh núi non hùng vĩ. Hành trình leo đến đỉnh Lũng Cú khá vất vả, nhưng trải nghiệm bạn nhận được rất xứng đáng&quot;. Ảnh: bi_tabu.\" />\r\n<p style=\"margin-left:0px; margin-right:0px\">Một địa điểm kh&aacute;c ở v&ugrave;ng n&uacute;i ph&iacute;a bắc được nhắc đến l&agrave; Lũng C&uacute; (H&agrave; Giang), điểm địa đầu đất nước. Blogger Fiona, chủ sở hữu trang Fiona travels from Asia, cho biết trải nghiệm đ&aacute;ng thử nhất khi đến Việt Nam l&agrave; đặt ch&acirc;n đến điểm cực bắc Việt Nam. Fiona mi&ecirc;u tả về kỷ niệm chinh phục Lũng C&uacute;: &ldquo;Nằm ở độ cao 1.470 m so với mực nước biển, cột cờ Lũng C&uacute; nghi&ecirc;m trang trong khung cảnh n&uacute;i non h&ugrave;ng vĩ. H&agrave;nh tr&igrave;nh leo đến đỉnh Lũng C&uacute; kh&aacute; vất vả, nhưng trải nghiệm bạn nhận được rất xứng đ&aacute;ng&rdquo;. Ảnh: bi_tabu.</p>\r\n</div>\r\n\r\n<p style=\"margin-left:0px; margin-right:0px; text-align:center\"><img alt=\"7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-5\" class=\"aligncenter size-full wp-image-333620\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-5.jpg\" style=\"border:0px; clear:both; display:block; font:inherit; height:auto; margin:0.4em auto 1.25em; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" /></p>\r\n\r\n<div class=\"aligncenter wp-caption\" id=\"attachment_333621\" style=\"-webkit-text-stroke-width:0px; border:0px; clear:both; color:#111111; display:block; font-family:&quot;Open Sans&quot;,&quot;Helvetica Neue&quot;,Calibri,&quot;Droid Sans&quot;,Helvetica,Arial,sans-serif; font-size:14px; font-stretch:inherit; font-style:normal; font-variant-caps:normal; font-variant-east-asian:inherit; font-variant-ligatures:normal; font-variant-numeric:inherit; font-weight:400; letter-spacing:normal; line-height:inherit; margin-bottom:0.4em; margin-left:1.25em; margin-right:1.25em; margin-top:0.4em; max-width:96%; orphans:2; padding:0px; text-align:center; text-decoration-color:initial; text-decoration-style:initial; text-decoration-thickness:initial; text-indent:0px; text-transform:none; vertical-align:baseline; white-space:normal; widows:2; width:1034px; word-spacing:0px\"><img alt=\"Blogger Nelson của trang du lịch The Scribes nhận xét: &quot;Ninh Bình đứng vị trí đầu trong danh sách các điểm nên đến ở Việt Nam&quot;. Đi thuyền khám phá Tràng An là trải nghiệm được Nelson đánh giá cao khi du lịch tại Ninh Bình. Thời gian tới, du khách có thể ghé Ninh Bình, đi thuyền qua sông Ngô Đồng ngắm những cánh đồng lúa chín, check-in ở Hang Múa nổi tiếng, khám phá Tam Cốc - Bích Động... Ảnh: sarahbakeroot, arissamc_.\" class=\"size-full wp-image-333621\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-6.jpg\" style=\"border:0px; display:block; font:inherit; height:auto; margin:0px auto; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" title=\"Blogger Nelson của trang du lịch The Scribes nhận xét: &quot;Ninh Bình đứng vị trí đầu trong danh sách các điểm nên đến ở Việt Nam&quot;. Đi thuyền khám phá Tràng An là trải nghiệm được Nelson đánh giá cao khi du lịch tại Ninh Bình. Thời gian tới, du khách có thể ghé Ninh Bình, đi thuyền qua sông Ngô Đồng ngắm những cánh đồng lúa chín, check-in ở Hang Múa nổi tiếng, khám phá Tam Cốc - Bích Động... Ảnh: sarahbakeroot, arissamc_.\" />\r\n<p style=\"margin-left:0px; margin-right:0px\">Blogger Nelson của trang du lịch The Scribes nhận x&eacute;t: &ldquo;Ninh B&igrave;nh đứng vị tr&iacute; đầu trong danh s&aacute;ch c&aacute;c điểm n&ecirc;n đến ở Việt Nam&rdquo;. Đi thuyền kh&aacute;m ph&aacute; Tr&agrave;ng An l&agrave; trải nghiệm được Nelson đ&aacute;nh gi&aacute; cao khi du lịch tại Ninh B&igrave;nh. Thời gian tới, du kh&aacute;ch c&oacute; thể gh&eacute; Ninh B&igrave;nh, đi thuyền qua s&ocirc;ng Ng&ocirc; Đồng ngắm những c&aacute;nh đồng l&uacute;a ch&iacute;n, check-in ở Hang M&uacute;a nổi tiếng, kh&aacute;m ph&aacute; Tam Cốc &ndash; B&iacute;ch Động&hellip; Ảnh: sarahbakeroot, arissamc_.</p>\r\n</div>\r\n\r\n<p style=\"margin-left:0px; margin-right:0px; text-align:center\"><img alt=\"7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-7\" class=\"aligncenter size-full wp-image-333622\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-7.jpg\" style=\"border:0px; clear:both; display:block; font:inherit; height:auto; margin:0.4em auto 1.25em; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" /></p>\r\n\r\n<div class=\"aligncenter wp-caption\" id=\"attachment_333623\" style=\"-webkit-text-stroke-width:0px; border:0px; clear:both; color:#111111; display:block; font-family:&quot;Open Sans&quot;,&quot;Helvetica Neue&quot;,Calibri,&quot;Droid Sans&quot;,Helvetica,Arial,sans-serif; font-size:14px; font-stretch:inherit; font-style:normal; font-variant-caps:normal; font-variant-east-asian:inherit; font-variant-ligatures:normal; font-variant-numeric:inherit; font-weight:400; letter-spacing:normal; line-height:inherit; margin-bottom:0.4em; margin-left:1.25em; margin-right:1.25em; margin-top:0.4em; max-width:96%; orphans:2; padding:0px; text-align:center; text-decoration-color:initial; text-decoration-style:initial; text-decoration-thickness:initial; text-indent:0px; text-transform:none; vertical-align:baseline; white-space:normal; widows:2; width:1034px; word-spacing:0px\"><img alt=\"Hội An (Quảng Nam) vốn là điểm đến được lòng nhiều du khách quốc tế. Một trong những trải nghiệm tại Hội An chưa nhiều người Việt để ý nhưng lại hấp dẫn khách Tây là may đồ tại tiệm may truyền thống. Blogger của trang Seenicwander gợi ý du khách nên một lần mặc đồ may ở phố cổ. &quot;Các hàng vải, tiệm may truyền thống là nét đep văn hóa đặc trưng lại Hội An. Bạn có thể tự chọn một khúc vải, đặt tiệm may theo số đo yêu cầu và nhận sản phẩm trong ngày&quot;, blogger trang Seenicwander viết. Ảnh: sheisnotlost, nomadvietnam.\" class=\"size-full wp-image-333623\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-8.jpg\" style=\"border:0px; display:block; font:inherit; height:auto; margin:0px auto; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" title=\"Hội An (Quảng Nam) vốn là điểm đến được lòng nhiều du khách quốc tế. Một trong những trải nghiệm tại Hội An chưa nhiều người Việt để ý nhưng lại hấp dẫn khách Tây là may đồ tại tiệm may truyền thống. Blogger của trang Seenicwander gợi ý du khách nên một lần mặc đồ may ở phố cổ. &quot;Các hàng vải, tiệm may truyền thống là nét đep văn hóa đặc trưng lại Hội An. Bạn có thể tự chọn một khúc vải, đặt tiệm may theo số đo yêu cầu và nhận sản phẩm trong ngày&quot;, blogger trang Seenicwander viết. Ảnh: sheisnotlost, nomadvietnam.\" />\r\n<p style=\"margin-left:0px; margin-right:0px\">Hội An (Quảng Nam) vốn l&agrave; điểm đến được l&ograve;ng nhiều du kh&aacute;ch quốc tế. Một trong những trải nghiệm tại Hội An chưa nhiều người Việt để &yacute; nhưng lại hấp dẫn kh&aacute;ch T&acirc;y l&agrave; may đồ tại tiệm may truyền thống. Blogger của trang Seenicwander gợi &yacute; du kh&aacute;ch n&ecirc;n một lần mặc đồ may ở phố cổ. &ldquo;C&aacute;c h&agrave;ng vải, tiệm may truyền thống l&agrave; n&eacute;t đep văn h&oacute;a đặc trưng lại Hội An. Bạn c&oacute; thể tự chọn một kh&uacute;c vải, đặt tiệm may theo số đo y&ecirc;u cầu v&agrave; nhận sản phẩm trong ng&agrave;y&rdquo;, blogger trang Seenicwander viết. Ảnh: sheisnotlost, nomadvietnam.</p>\r\n</div>\r\n\r\n<div class=\"aligncenter wp-caption\" id=\"attachment_333624\" style=\"-webkit-text-stroke-width:0px; border:0px; clear:both; color:#111111; display:block; font-family:&quot;Open Sans&quot;,&quot;Helvetica Neue&quot;,Calibri,&quot;Droid Sans&quot;,Helvetica,Arial,sans-serif; font-size:14px; font-stretch:inherit; font-style:normal; font-variant-caps:normal; font-variant-east-asian:inherit; font-variant-ligatures:normal; font-variant-numeric:inherit; font-weight:400; letter-spacing:normal; line-height:inherit; margin-bottom:0.4em; margin-left:1.25em; margin-right:1.25em; margin-top:0.4em; max-width:96%; orphans:2; padding:0px; text-align:center; text-decoration-color:initial; text-decoration-style:initial; text-decoration-thickness:initial; text-indent:0px; text-transform:none; vertical-align:baseline; white-space:normal; widows:2; width:1034px; word-spacing:0px\"><img alt=\"Blogger Rose Munday lại cho rằng khám phá những bãi biển yên bình, nguyên sơ ở Quy Nhơn (Bình Định) là trải nghiệm đáng thử khi tới Việt Nam. Đến đây, du khách có thể tận hưởng kỳ nghỉ dưỡng ven biển, khám phá cuộc sống dân chài, thưởng thức hải sản địa phương, Rose Munday gợi ý. Ảnh: Nguyễn Quang Ngọc.\" class=\"size-full wp-image-333624\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-9.jpg\" style=\"border:0px; display:block; font:inherit; height:auto; margin:0px auto; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" title=\"Blogger Rose Munday lại cho rằng khám phá những bãi biển yên bình, nguyên sơ ở Quy Nhơn (Bình Định) là trải nghiệm đáng thử khi tới Việt Nam. Đến đây, du khách có thể tận hưởng kỳ nghỉ dưỡng ven biển, khám phá cuộc sống dân chài, thưởng thức hải sản địa phương, Rose Munday gợi ý. Ảnh: Nguyễn Quang Ngọc.\" />\r\n<p style=\"margin-left:0px; margin-right:0px\">Blogger Rose Munday lại cho rằng kh&aacute;m ph&aacute; những b&atilde;i biển y&ecirc;n b&igrave;nh, nguy&ecirc;n sơ ở Quy Nhơn (B&igrave;nh Định) l&agrave; trải nghiệm đ&aacute;ng thử khi tới&nbsp; du lịch Việt Nam. Đến đ&acirc;y, du kh&aacute;ch c&oacute; thể tận hưởng kỳ nghỉ dưỡng ven biển, kh&aacute;m ph&aacute; cuộc sống d&acirc;n ch&agrave;i, thưởng thức hải sản địa phương, Rose Munday gợi &yacute;. Ảnh: Nguyễn Quang Ngọc.</p>\r\n</div>\r\n\r\n<p style=\"margin-left:0px; margin-right:0px; text-align:center\"><img alt=\"7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-10\" class=\"aligncenter size-full wp-image-333625\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-10.jpg\" style=\"border:0px; clear:both; display:block; font:inherit; height:auto; margin:0.4em auto 1.25em; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" /></p>\r\n\r\n<div class=\"aligncenter wp-caption\" id=\"attachment_333626\" style=\"-webkit-text-stroke-width:0px; border:0px; clear:both; color:#111111; display:block; font-family:&quot;Open Sans&quot;,&quot;Helvetica Neue&quot;,Calibri,&quot;Droid Sans&quot;,Helvetica,Arial,sans-serif; font-size:14px; font-stretch:inherit; font-style:normal; font-variant-caps:normal; font-variant-east-asian:inherit; font-variant-ligatures:normal; font-variant-numeric:inherit; font-weight:400; letter-spacing:normal; line-height:inherit; margin-bottom:0.4em; margin-left:1.25em; margin-right:1.25em; margin-top:0.4em; max-width:96%; orphans:2; padding:0px; text-align:center; text-decoration-color:initial; text-decoration-style:initial; text-decoration-thickness:initial; text-indent:0px; text-transform:none; vertical-align:baseline; white-space:normal; widows:2; width:1034px; word-spacing:0px\"><img alt=\"Khám phá địa đạo Củ Chi (TP.HCM) là trải nghiệm được blogger Delilah đánh giá cao khi tới Việt Nam. Đến đây, du khách được hiểu hơn về lịch sử Việt Nam, mục sở thị đường hầm khổng lồ dưới lòng đất. Chia sẻ trên trang cá nhân, Delilah cho biết sau khi đến địa đạo Củ Chi anh cảm thấy may mắn khi được sinh ra trong thời bình. Ảnh: fanch, ag.ogieglo94.\" class=\"size-full wp-image-333626\" src=\"https://cdn3.ivivu.com/2020/06/7-dieu-khach-tay-hung-thu-khi-du-lich-tai-viet-nam-ivivu-11.jpg\" style=\"border:0px; display:block; font:inherit; height:auto; margin:0px auto; max-width:100%; padding:0px; vertical-align:baseline; width:1024px\" title=\"Khám phá địa đạo Củ Chi (TP.HCM) là trải nghiệm được blogger Delilah đánh giá cao khi tới Việt Nam. Đến đây, du khách được hiểu hơn về lịch sử Việt Nam, mục sở thị đường hầm khổng lồ dưới lòng đất. Chia sẻ trên trang cá nhân, Delilah cho biết sau khi đến địa đạo Củ Chi anh cảm thấy may mắn khi được sinh ra trong thời bình. Ảnh: fanch, ag.ogieglo94.\" />\r\n<p style=\"margin-left:0px; margin-right:0px\">Kh&aacute;m ph&aacute; địa đạo Củ Chi (TP.HCM) l&agrave; trải nghiệm được blogger Delilah đ&aacute;nh gi&aacute; cao khi tới&nbsp;<strong>du lịch Việt Nam</strong>. Đến đ&acirc;y, du kh&aacute;ch được hiểu hơn về lịch sử Việt Nam, mục sở thị đường hầm khổng lồ dưới l&ograve;ng đất. Chia sẻ tr&ecirc;n trang c&aacute; nh&acirc;n, Delilah cho biết sau khi đến địa đạo Củ Chi anh cảm thấy may mắn khi được sinh ra trong thời b&igrave;nh. Ảnh: fanch, ag.ogieglo94.</p>\r\n</div>\r\n', '2022-12-05 23:11:11', 1, 1, 'hinh-anh-Viet-Nam-mytour-8.jpg', 1),
(22, 'Du Lịch Đà Nẵng : Giới thiệu về Bà Nà Hills', '<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Giải thích 1\" title=\"Giải thích 1\">Giải thích 1</a></h3>\r\n\r\n<p>xưa kia khi người Pháp lần đầu tiên đặt chân tới đây, nhìn thấy trên núi có rất nhiều chuối nên gọi là ”banane” và lâu dần theo cách gọi chệch đi của người Việt mà có tên là Bà Nà.</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Giải thích 2\" title=\"Giải thích 2\">Giải thích 2</a></h3>\r\n\r\n<p style=\"text-align:justify\">Dựa theo nhà văn Nguyên Ngọc, theo tiếng người Katu từ Bà Nà mang nghĩa ‘’nhà của tôi’’</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Giải thích 3\" title=\"Giải thích 3\">Giải thích 3</a></h3>\r\n\r\n<p style=\"text-align:justify\">Do người dân địa phương đặt tên mà thành. Từ ‘’Bà’’ ngụ ý những con vật linh thiêng và ‘’Nà’’ chính là khoảng đất rộng tại những triền núi</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Giải thích 4\" title=\"Giải thích 4\">Giải thích 4</a></h3>\r\n\r\n<p>Từ Bà Nà là tên gọi tắt của thánh mẫu Y A Na hoặc bà Ponaga.</p>\r\n\r\n<h2><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Những điểm vui chơi giải trí tại Bà Nà Hills\" title=\"Những điểm vui chơi giải trí tại Bà Nà Hills\">Những điểm vui chơi giải trí tại Bà Nà Hills</a></h2>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Cầu Vàng Bà Nà hills\" title=\"Cầu Vàng Bà Nà hills\">Cầu Vàng Bà Nà hills</a></h3>\r\n\r\n<p><img alt=\"Cầu Vàng Bà Nà hills\" src=\"https://danangsensetravel.com/view/at_gioi-thieu-ve-ba-na-hills_33c83ee5467dfc9823eaf3b6503f504b.jpg\" title=\"Cầu Vàng Bà Nà hills\" /></p>\r\n\r\n<p>Được giới thiệu từ tháng 06/2018, câu cầu Vàng đã trở thành một hiện tượng thu hút sự chú ý của đông đảo người đam mê du lịch trong nước cũng như quốc tế. Và mới đây, tạp chí Times còn bình chọn cây cầu Vàng thuộc Top 10 điểm đến hấp dẫn nhất hành tinh năm 2018.</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Fantasy Park\" title=\"Fantasy Park\">Fantasy Park</a></h3>\r\n\r\n<p>Với diện tích ‘’khủng’’ 21.000m2, Fantasy Park được biết đến là là khu trò chơi giải trí  trong nhà lớn nhất khu vực châu Á sẽ mang đến cho du khách những phút giây thật vui vẻ cùng nhiều trò chơi mới lạ và thú vị. Fantasy chia làm 3 tầng với hàng ngàn trò chơi từ nhẹ nhàng tới mạo hiểm với tổng số hơn 90 trò chơi miễn phí như phi công Skiver, tháp rơi tự do, công viên kỷ Jura, ngôi nhà ma ám, đua xe Outrun, cối xay gió, khu rừng thần tiên, vòng đua tử thần 4D,… Ngoài ra, ở Fantasy Park còn có 3 khu ẩm thực rất phong phú phục vụ nhiều món ăn Á, Âu cùng với một khu quầy hàng lưu niệm nơi bày bán nhiều sản phẩm đa dạng.</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Vườn hoa Le Jardin d’Amour\" title=\"Vườn hoa Le Jardin d’Amour\">Vườn hoa Le Jardin d’Amour</a></h3>\r\n\r\n<p><img alt=\"Vườn hoa Le Jardin d’Amour\" src=\"https://danangsensetravel.com/view-800/at_gioi-thieu-ve-ba-na-hills_af7f7fd4cd73c40b62e8a2977276d29b.jpg\" title=\"Vườn hoa Le Jardin d’Amour\" /></p>\r\n\r\n<p>Là một vườn hoa với diện tích khổng lồ 8.206m2 nằm tại phía Đông của núi Bà Nà. Vườn hoa thu hút du khách bởi khí hậu luôn mát mẻ dễ chịu và muôn vàn loài hoa đẹp. Đặc biệt, ở đây gồm có 9 khu vườn nhỏ với kiến trúc độc đáo khác nhau là vườn Suối Mơ, vườn Địa Đàng, vườn Bí Ẩn, vườn Uyên Ương, vườn Suy Tưởng, vườn Thần Thoại, vườn, Ký Ức, vườn Thiêng và vườn nho. Đến với vườn hoa phong cách Pháp ‘’Le Jardin d’Amour’’, du khách còn được đắm say với nhiều loài hoa quý hiếm và đầy màu sắc như cẩm tú cầu, lavender,  Xác pháo Nữ Hoàng,  đào chuông,  cúc bách nhật…</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Chùa Linh Ứng\" title=\"Chùa Linh Ứng\">Chùa Linh Ứng</a></h3>\r\n\r\n<p>Được xây dựng trên đỉnh Bà Nà, chùa Linh Ứng có độ cao đạt 1500m đã trở thành du lịch tâm linh nổi tiếng mà du khách không nên bỏ qua. Ngôi chùa này có phong cách kiến trúc giống với các loại chùa Việt Nam với mái ngói cong, diện tích khoảng sân rộng lót bằng đá và phía trước trồng cây thông có 3 loại lá khác nhau. Đặc biệt, du khách đến đây sẽ vô cùng ấn tượng trước tượng Đức Bổn Sư Thích Ca Mâu cao 27m, thiền định trên một đài hoa sen và bên dưới có 8 bức phù điêu chạm khắc tinh tế, tất cả tái hiện lại cuộc đời của Đức Phật Thích ca. Ngoài ra, du khách khi đến đây còn được ngắm nhìn toàn cảnh thành phố Đà Nẵng, bán đảo Sơn Trà, bãi biển Mỹ Khê…</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Ngôi làng Pháp\" title=\"Ngôi làng Pháp\">Ngôi làng Pháp</a></h3>\r\n\r\n<p><img alt=\"Ngôi làng Pháp\" src=\"https://danangsensetravel.com/view/at_gioi-thieu-ve-ba-na-hills_39437619b1b47774264d940cd542e09c.jpg\" title=\"Ngôi làng Pháp\" /></p>\r\n\r\n<p>Đến với ngôi làng Pháp, các bạn sẽ được trải nghiệm một nước Pháp đầy lãng mạn và cổ điển với dấu ấn phong cách từ thế kỷ thứ 20 với những công trình kiến trúc độc đáo như hàng loạt lâu đài lãng mạn, Thánh đường St Denis, Brittany, ngôi làng nhỏ nhắn Apremont sur Allier, quảng trường, khách sạn, làng mạc, thị trấn… Ngoài ra, tại ngôi làng Pháp xinh đẹp này còn sở hữu nhiều dịch vụ tiện ích hiện đại như: bể bơi khép kín, hệ thống spa đẳng cấp, phòng hội nghị, khu vực mua sắm, café quán bar, chương trình giải trí bốn mua dành cho trẻ em và người lớn…</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Tháp Nghinh Phong Tự\" title=\"Tháp Nghinh Phong Tự\">Tháp Nghinh Phong Tự</a></h3>\r\n\r\n<p>Là một trong những địa điểm du lịch nhận được sự chú ý của đông đảo du khách khi đến với Bà Nà Hills. Tháp Nghinh Phong Tự được xây dựng với lối kiến trúc cổ kính, uy nghi cao tới 9 tầng, trong đó mỗi tầng đều được đặt 4 chiếc chuông đồng tại 4 góc với mục đích biểu tượng cho âm vang linh thiêng. Bên cạnh đó, du khách khi đứng từ trên tháp sẽ chiêm ngưỡng được cả một khung cảnh núi non hùng vĩ, hít hà bầu không khí trong lành và có được những phút giây thư giãn yên bình.</p>\r\n\r\n<h3><a href=\"https://danangsensetravel.com/gioi-thieu-ve-ba-na-hills-n.html#\" name=\"Thác Tóc Tiên\" title=\"Thác Tóc Tiên\">Thác Tóc Tiên</a></h3>\r\n', '2022-12-05 23:12:21', 2, 1, 'hinh-anh-Viet-Nam-mytour-6.jpg', 1),
(23, 'Một Ngày Ở Bà Nà Hills - Đi Đâu, Chơi Gì, Ăn Gì?', '<div class=\"p-txt\"><strong><em>Đi du lịch Đ&agrave; Nẵng m&agrave; chưa check-in tại khu du lịch B&agrave; N&agrave; Hills l&agrave; thiếu s&oacute;t v&ocirc; c&ugrave;ng lớn!</em></strong></div>\r\n\r\n<div class=\"p-txt\">Bạn đang c&oacute; kế hoạch du lịch B&agrave; N&agrave; Hills tự t&uacute;c? Tất cả những hoạt động tham quan, vui chơi, ăn uống đều được Klook tổng hợp trong b&agrave;i viết n&agrave;y, #teamKlook tham khảo nh&eacute;!</div>\r\n\r\n<h2><strong>B&agrave; N&agrave; Hills ở đ&acirc;u?</strong></h2>\r\n\r\n<div class=\"p-txt\">B&agrave; N&agrave; Hills toạ lạc ở huyện H&ograve;a Vang, nằm c&aacute;ch trung t&acirc;m th&agrave;nh phố Đ&agrave; Nẵng khoảng 25km về ph&iacute;a T&acirc;y Nam, nằm ở độ cao 1487m so với mực bước biển. Đ&acirc;y được xem &agrave; thi&ecirc;n đường nghỉ dưỡng đẳng cấp Ch&acirc;u &Acirc;u giữa l&ograve;ng th&agrave;nh phố Đ&agrave; Nẵng, h&agrave;ng năm, B&agrave; N&agrave; Hills thu h&uacute;t h&agrave;ng triệu lượt kh&aacute;ch trong v&agrave; ngo&agrave;i nước gh&eacute; thăm.</div>\r\n\r\n<h2><strong>N&ecirc;n đi B&agrave; N&agrave; v&agrave;o thời gian n&agrave;o?&nbsp;</strong></h2>\r\n\r\n<div class=\"header-banner-comp\">\r\n<div style=\"height:0; padding-bottom:66.75531914893617%; width:100%\"><img alt=\"ba-na-hills\" src=\"https://res.klook.com/image/upload/fl_lossy.progressive,q_85/c_fill,w_680/v1596022863/blog/ljinwuevicmkjjnh6zf8.webp\" style=\"opacity:1\" /></div>\r\n</div>\r\n\r\n<div class=\"p-txt\">Thời tiết v&agrave; kh&iacute; hậu tại</div>\r\n\r\n<div class=\"p-txt\">thay đổi li&ecirc;n tục, s&aacute;ng mưa, trưa nắng, chiều sương m&ugrave;. V&igrave; vậy, bạn cần ch&uacute; &yacute; xem dự b&aacute;o thời tiết trước khi đi để c&oacute; được một ng&agrave;y vui chơi trọn vẹn ở B&agrave; N&agrave; Hills nh&eacute;!</div>\r\n\r\n<div class=\"p-txt\">Để tr&aacute;nh hiện tượng mưa v&agrave; sương m&ugrave;, thời gian đẹp nhất để bạn đến đ&acirc;y l&agrave; từ th&aacute;ng 4 đến th&aacute;ng 9. V&agrave;o thời gian n&agrave;y, trời &iacute;t mưa, thời tiết đẹp, gi&uacute;p bạn bắt trọn g&oacute;c đẹp ở mọi nơi. Hoặc nếu bạn muốn tham gia c&aacute;c lễ hội ở B&agrave; N&agrave; Hills, h&atilde;y đi v&agrave;o m&ugrave;a xu&acirc;n (th&aacute;ng 1 - th&aacute;ng 3) v&agrave; m&ugrave;a đ&ocirc;ng (th&aacute;ng 10 - th&aacute;ng 12), nhưng h&atilde;y nhớ mang theo &aacute;o ấm v&agrave; &ocirc; d&ugrave; nh&eacute;, v&igrave; thời gian n&agrave;y ở B&agrave; N&agrave; kh&aacute; lạnh v&agrave; ẩm ướt đấy!</div>\r\n\r\n<h2><strong>Hướng dẫn c&aacute;ch đi đến B&agrave; N&agrave; Hills Đ&agrave; Nẵng</strong></h2>\r\n\r\n<h3><strong>1. Đi B&agrave; N&agrave; Hills bằng xe bu&yacute;t</strong></h3>\r\n\r\n<div class=\"p-txt\">Nằm trong địa phận th&agrave;nh phố Đ&agrave; Nẵng, hầu hết c&aacute;c du kh&aacute;ch khi đến Đ&agrave; Nẵng sẽ gh&eacute; B&agrave; N&agrave; Hills. Để dễ d&agrave;ng đến B&agrave; N&agrave; Hills từ trung t&acirc;m th&agrave;nh phố Đ&agrave; Nẵng, bạn c&oacute; thể bắt xe bu&yacute;t trung chuyển của B&agrave; N&agrave; Hills, xe sẽ dừng ở một số trạm cố định,&nbsp; bạn cần theo d&otilde;i th&ocirc;ng tin tr&ecirc;n website để biết được thời gian v&agrave; c&aacute;c trạm dừng. Hoặc nếu bạn đi nh&oacute;m nhiều người, h&atilde;y thu&ecirc; xe ri&ecirc;ng đưa đ&oacute;n tận kh&aacute;ch sạn.</div>\r\n\r\n<h3><strong>2. Thu&ecirc; xe đi B&agrave; N&agrave; Hills</strong></h3>\r\n\r\n<div class=\"p-txt\">Thu&ecirc; xe đi</div>\r\n\r\n<div class=\"p-txt\">l&agrave; giải ph&aacute;p nhanh ch&oacute;ng v&agrave; tiện lợi nhất, d&agrave;nh cho người đi du lịch theo nh&oacute;m từ ba người trở l&ecirc;n. Tuỳ theo nhu cầu m&agrave; bạn c&oacute; thể chọn c&aacute;ch thu&ecirc; xe ri&ecirc;ng theo g&oacute;i 1 chiều hoặc khứ hồi đều được. Giữa tiết trời Đ&agrave; Nẵng n&oacute;ng nực, c&ograve;n g&igrave; tuyệt vời hơn l&agrave; được &quot;chill&quot; tr&ecirc;n xe hơi hiện đại, c&oacute; m&aacute;y lạnh m&aacute;t mẻ v&agrave; được t&agrave;i xế &quot;thổ địa&quot; được đến nơi an to&agrave;n, #teamKlook nhỉ?</div>\r\n\r\n<div class=\"p-txt\">Bạn c&oacute; thể đặt dịch vụ</div>\r\n\r\n<div class=\"p-txt\">tại Klook với gi&aacute; tiết kiệm. Đừng bỏ qua nh&eacute;!</div>\r\n\r\n<div class=\"activity-card\">\r\n<div class=\"bury-point-comp\">\r\n<div class=\"klk-standard-card--white\" style=\"-moz-box-direction:normal; -moz-box-orient:vertical; background-position:center center; background-repeat:no-repeat; background-size:cover; border-radius:6px; display:flex; flex-direction:column; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; overflow:hidden; padding:0px; width:100%\">\r\n<div style=\"background-image:url(&quot;https://res.klook.com/image/upload/q_90/c_scale,w_420/activities/jtcg9ogkvuaiiglphnae.jpg&quot;); background-position:center center; background-repeat:no-repeat; background-size:cover; border-radius:6px 6px 0px 0px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; overflow:hidden; padding:0px 0px 56.4972%; position:relative; width:100%\">\r\n<div class=\"klk-promo-tag-large-wrapper\" style=\"margin-bottom:0px; margin-left:12px; margin-right:12px; margin-top:0px; padding:0px; position:absolute; top:10px; width:100%\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"activity-card\">\r\n<div class=\"bury-point-comp\">\r\n<div class=\"klk-standard-card--white\" style=\"-moz-box-direction:normal; -moz-box-orient:vertical; background-position:center center; background-repeat:no-repeat; background-size:cover; border-radius:6px; display:flex; flex-direction:column; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; overflow:hidden; padding:0px; width:100%\">\r\n<div style=\"-moz-box-direction:normal; -moz-box-orient:vertical; background-color:#ffffff; background-position:center center; background-repeat:no-repeat; background-size:cover; border-radius:0px; display:flex; flex-direction:column; height:90px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; overflow:hidden; padding:10px 16px 0px; width:100%\">\r\n<div class=\"klk-card-text_content klk-card-text_content--left\" style=\"margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding:0px; width:100%\"><span style=\"color:#212121; font-size:14px\">Thu&ecirc; Xe Đi B&agrave; N&agrave; Hills Từ Đ&agrave; Nẵng</span></div>\r\n\r\n<div style=\"-moz-box-align:center; align-items:center; display:flex; height:16px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; overflow:hidden; padding:4px 0px 0px; width:100%\">\r\n<div class=\"klk-card-text_content klk-card-text_content--left\" style=\"margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding:0px; width:-moz-fit-content\"><span style=\"color:#f58b1b; font-size:12px\">★ 4.5</span></div>\r\n\r\n<div class=\"klk-card-text_content klk-card-text_content--left\" style=\"margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding:0px; width:-moz-fit-content\"><span style=\"color:#999999; font-size:12px\">(2,114)</span></div>\r\n\r\n<div class=\"klk-card-text_content klk-card-text_content--left\" style=\"margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding:0px; width:-moz-fit-content\"><span style=\"color:#999999; font-size:12px\">&bull; </span></div>\r\n\r\n<div class=\"klk-card-text_content klk-card-text_content--left\" style=\"margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding:0px; width:-moz-fit-content\"><span style=\"color:#999999; font-size:12px\">10K+ Đ&atilde; đặt</span></div>\r\n</div>\r\n\r\n<div class=\"klk-card-general-tag\" style=\"margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding:7px 0px 0px; width:100%\"><span style=\"background-color:#e9f8f1; color:#16aa77\">B&aacute;n chạy </span><span style=\"background-color:#f5f5f5; color:#757575\">X&aacute;c nhận tức thời </span></div>\r\n</div>\r\n\r\n<div style=\"-moz-box-direction:reverse; -moz-box-orient:vertical; background-color:#ffffff; background-position:center center; background-repeat:no-repeat; background-size:cover; border-radius:0px 0px 6px 6px; display:flex; flex-direction:column-reverse; height:54px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; overflow:hidden; padding:0px 16px 12px; width:100%\">\r\n<div class=\"klk-card-price-desc\" style=\"margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding:0px; width:100%\">\r\n<div class=\"klk-card-price-desc_content\">\r\n<p>&nbsp;</p>\r\n\r\n<p>Từ <strong>₫ 360,000</strong></p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div style=\"background-color:#e0e0e0; background-position:center center; background-repeat:no-repeat; background-size:cover; border-radius:0px; display:flex; height:1px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; overflow:hidden; padding:0px; width:100%\">&nbsp;</div>\r\n\r\n<div class=\"klk-card-text_content klk-card-text_content--center\" style=\"height:48px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding:0px; width:100%\"><span style=\"color:#ff5722; font-size:14px\">Book Now ﹥</span></div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<h3><strong>3. Đi c&aacute;p treo l&ecirc;n đỉnh n&uacute;i B&agrave; N&agrave;&nbsp;</strong></h3>\r\n\r\n<div class=\"header-banner-comp\">\r\n<div style=\"height:0; padding-bottom:66.31439894319684%; width:100%\"><img alt=\"ba-na-hills\" src=\"https://res.klook.com/image/upload/fl_lossy.progressive,q_85/c_fill,w_680/v1596022826/blog/btsrdkd0jv8bx8knlhql.webp\" style=\"opacity:1\" /></div>\r\n</div>\r\n\r\n<div class=\"p-txt\"><em>C&oacute; thể bạn chưa biết: hệ thống c&aacute;p treo của B&agrave; N&agrave; Hills được CNN b&igrave;nh chọn l&agrave; 1 trong 10 tuyến c&aacute;p treo ấn tượng nhất thế giới.</em></div>\r\n\r\n<div class=\"p-txt\">Để v&agrave;o khu c&ocirc;ng vi&ecirc;n giải tr&iacute; của B&agrave; N&agrave; Hill, bạn sẽ được trải nghiệm hệ thống c&aacute;p treo cao v&agrave; d&agrave;i nhất thế giới, d&agrave;i 5,801 m&eacute;t v&agrave; cao 1.368 m&eacute;t. Hệ thống c&aacute;p treo được x&acirc;y dựng theo ti&ecirc;u chuẩn ch&acirc;u &Acirc;u, lập đến 4 kỷ lục thế giới n&agrave;y sẽ mang đến cho bạn &quot;view&quot;cực xịn s&ograve; về th&agrave;nh phố Đ&agrave; Nẵng v&agrave; to&agrave;n cảnh Khu vực B&agrave; N&agrave; Hills.&nbsp;</div>\r\n', '2022-12-05 23:14:59', 2, 1, 'cau-vang-ba-na-hills.jpg', 1),
(24, 'Cẩm nang kinh nghiệm du lịch Ba Na Hills chi tiết từ A – Z', '<h2><strong>1. N&ecirc;n đi du lịch Ba Na Hills v&agrave;o thời điểm n&agrave;o?</strong></h2>\r\n\r\n<p>Với cảnh sắc thi&ecirc;n nhi&ecirc;n tươi đẹp v&agrave; kh&iacute; hậu m&aacute;t mẻ, du kh&aacute;ch c&oacute; thể đi du lịch Ba Na Hills v&agrave;o bất cứ m&ugrave;a n&agrave;o trong năm. Tuy nhi&ecirc;n, c&oacute; những thời điểm đặc biệt th&iacute;ch hợp cho việc du lịch Ba Na Hills:</p>\r\n\r\n<ul>\r\n	<li><strong>Thời gian từ th&aacute;ng 4 đến th&aacute;ng 10:&nbsp;</strong>Đ&acirc;y l&agrave; khoảng thời gian m&ugrave;a h&egrave; trong năm. Đến B&agrave; N&agrave; v&agrave;o thời gian n&agrave;y, bạn sẽ được tận hưởng kh&iacute; hậu 4 m&ugrave;a trong 1 ng&agrave;y độc đ&aacute;o, ngắm nh&igrave;n trọn vẹn nhất ti&ecirc;n cảnh Ba Na Hills m&agrave; kh&ocirc;ng sợ sương m&ugrave; che tầm mắt.</li>\r\n	<li>Đọc th&ecirc;m:<strong><a href=\"https://banahills.sunworld.vn/tin-tuc/thoi-tiet-ba-na-hills\">Kh&iacute; hậu B&agrave; N&agrave; c&oacute; g&igrave; đặc biệt ?</a></strong></li>\r\n	<li><strong>Thời gian lễ hội:&nbsp;</strong>Ba Na Hills được mệnh danh l&agrave; &ldquo;điểm đến của sự kiện v&agrave; lễ hội h&agrave;ng đầu khu vực&rdquo;. C&aacute;c lễ hội thường tr&ugrave;ng với c&aacute;c dịp lễ, Tết với nhiều trải nghiệm độc đ&aacute;o, mới lạ. V&agrave;o thời gian n&agrave;y, B&agrave; N&agrave; được trang ho&agrave;ng lộng lẫy, kh&ocirc;ng kh&iacute; cũng trở n&ecirc;n nhộn nhịp v&agrave; vui vẻ hơn.</li>\r\n</ul>\r\n\r\n<p><img alt=\"Những quả bí ngô được trang trí bắt mắt trong lễ hội Halloween tại Ba Na Hills.\" class=\"size-full wp-image-5777\" src=\"https://banahills.sunworld.vn/wp-content/uploads/2020/03/le-hoi-tren-ba-na.png\" style=\"height:648px; width:960px\" /></p>\r\n\r\n<p>Những quả b&iacute; ng&ocirc; được trang tr&iacute; bắt mắt trong lễ hội Halloween tại Ba Na Hills.</p>\r\n\r\n<h2><strong><a href=\"https://banahills.sunworld.vn/su-kien/vu-hoi-anh-duong-ba-na-hills.html\">Vũ hội &Aacute;nh Dương B&agrave; N&agrave; &ndash; Ấn tượng trong từng bộ trang phục</a></strong></h2>\r\n\r\n<h2><strong>2. Gi&aacute; v&eacute; Ba Na Hills</strong></h2>\r\n\r\n<p>Cập nhật mới nhất gi&aacute; v&eacute; B&agrave; N&agrave; cho du kh&aacute;ch tham khảo như sau:</p>\r\n\r\n<p><strong>Gi&aacute; v&eacute; d&agrave;nh cho kh&aacute;ch ngoại tỉnh:</strong></p>\r\n\r\n<table style=\"width:602px\">\r\n	<tbody>\r\n		<tr>\r\n			<td rowspan=\"2\"><strong>&nbsp;</strong><strong>Đối tượng</strong></td>\r\n			<td colspan=\"2\">&nbsp;01/01/2020 &ndash; 31/03/2020</td>\r\n			<td colspan=\"2\">01/04/2020-31/12/2020</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Người lớn</td>\r\n			<td>Trẻ em(1m-1,4m)</td>\r\n			<td>Người lớn</td>\r\n			<td>Trẻ em(1-1,4m )</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Gi&aacute; v&eacute;</strong></td>\r\n			<td>750,000</td>\r\n			<td>600,000</td>\r\n			<td>800,000</td>\r\n			<td>650,000</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Gi&aacute; v&eacute; d&agrave;nh cho kh&aacute;ch Đ&agrave; Nẵng:</strong></p>\r\n\r\n<table style=\"width:602px\">\r\n	<tbody>\r\n		<tr>\r\n			<td rowspan=\"2\"><strong>&nbsp;</strong><strong>Gi&aacute; v&eacute;</strong></td>\r\n			<td colspan=\"2\">&nbsp;01/01/2020 &ndash; 31/03/2020</td>\r\n			<td colspan=\"2\">01/04/2020-31/12/2020</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Người lớn</td>\r\n			<td>Trẻ em(1M-1M40 )</td>\r\n			<td>Người lớn</td>\r\n			<td>Trẻ em(1M-1M40 )</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Gi&aacute; c&ocirc;ng bố</strong></td>\r\n			<td>450,000</td>\r\n			<td>350,000</td>\r\n			<td>500,000</td>\r\n			<td>400,000</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Lưu &yacute;:</strong></p>\r\n\r\n<ul>\r\n	<li>Trẻ em cao dưới 1m được miễn ph&iacute;</li>\r\n	<li>Người d&acirc;n th&agrave;nh phố Đ&agrave; Nẵng khi mua v&eacute; cần xuất tr&igrave;nh CMND/giấy khai sinh để nhận được ưu đ&atilde;i.</li>\r\n</ul>\r\n\r\n<p><strong>V&eacute; Ba Na Hills bao gồm:</strong></p>\r\n\r\n<ul>\r\n	<li style=\"list-style-type:none\">\r\n	<ul>\r\n		<li>V&eacute; v&agrave;o cửa khu du lịch, v&eacute; c&aacute;p treo B&agrave; N&agrave;, miễn ph&iacute; hầu hết tr&ograve; chơi ở khu vui chơi Fantasy Park, t&agrave;u hoả leo n&uacute;i</li>\r\n		<li>Tham quan vườn hoa Le Jardin D&rsquo;Amour&rsquo;, hầm rượu Debay, Cầu V&agrave;ng, L&agrave;ng Ph&aacute;p, khu T&acirc;m linh.</li>\r\n		<li>Gi&aacute; đ&atilde; bao g&ocirc;̀m 10% thuế VAT (thuế gi&aacute; trị gia tăng)</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<p><a href=\"https://ticket.sunworld.vn/\"><img alt=\"\" class=\"aligncenter wp-image-7395\" id=\"dat-ve-ba-na\" src=\"https://banahills.sunworld.vn/wp-content/uploads/2020/03/banahill.gif\" style=\"height:83px; width:328px\" /></a></p>\r\n\r\n<p><strong>V&eacute; Ba Na Hills kh&ocirc;ng bao gồm:</strong></p>\r\n\r\n<ul>\r\n	<li>Dịch vụ ăn uống ở Ba Na Hills</li>\r\n	<li>Ph&iacute; tham quan Bảo t&agrave;ng S&aacute;p (100.000 VNĐ/người lớn v&agrave; trẻ em tr&ecirc;n 1,4m)</li>\r\n	<li>Tr&ograve; chơi tr&uacute;ng thưởng tại Fantasy Park</li>\r\n</ul>\r\n\r\n<p>Du kh&aacute;ch c&oacute; thể mua v&eacute; Ba Na Hills bằng c&aacute;ch mua trực tiếp tại c&aacute;c quầy b&aacute;n v&eacute; hoặc mua Online&nbsp;<a href=\"https://ticket.sunworld.vn/#/home?step=2&amp;facility=1\"><strong>TẠI Đ&Acirc;Y.</strong></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tham khảo th&ecirc;m:&nbsp;<a href=\"https://banahills.sunworld.vn/tin-tuc/dat-ve-ba-na-hills-2020-nhanh-chong-tiet-kiem.html\">Mẹo Đặt V&eacute; C&aacute;ch mua v&eacute; Ba Na Hills khuyến mại gi&aacute; rẻ nhất</a></strong></p>\r\n', '2022-12-05 23:16:11', 2, 1, 'le-hoi-tren-ba-na.png', 1);
INSERT INTO `bai_viet` (`id`, `ten_bai_viet`, `noi_dung`, `thoi_gian_tao`, `id_the_loai`, `trang_thai`, `anh_tieu_de`, `id_nguoi_dung`) VALUES
(25, 'Check-in liền tay khu nghỉ dưỡng New World Phú Quốc mới toanh ở bãi Kem', '<h4>Check-in liền tay khu nghỉ dưỡng New World Phú Quốc mới toanh ở bãi Kem</h4>\r\n\r\n<p><img alt=\"khu-nghi-duong-new-world-phu-quoc-ivivu-1\" class=\"aligncenter size-full wp-image-352573\" src=\"https://cdn3.ivivu.com/2021/10/khu-nghi-duong-new-world-phu-quoc-ivivu-1.jpg\" style=\"height:803px; width:1200px\" />Với 375 biệt thự có thiết kế pha trộn đầy tinh tế giữa truyền thống và hiện đại, <em><a href=\"https://www.ivivu.com/khach-san-phu-quoc/khu-nghi-duong-new-world-phu-quoc?utm_source=blog_ivivu&utm_medium=post_link&utm_campaign=internal\" target=\"_blank\">khu nghỉ dưỡng New World Phú Quốc</a></em> giống như một làng chài ven biển. Mỗi biệt thự gồm từ 3 – 4 phòng ngủ, sở hữu không gian mở thoáng đãng, hồ bơi riêng và khoảng sân vườn rộng rãi. Bên trong các biệt thự, lối trang trí thanh thoát với các vật liệu thân thuộc của làng quê Việt như gỗ, mây, tre… tạo nên một không gian thanh lịch, sang trọng mà vẫn rất đỗi gần gũi, ấm cúng.</p>\r\n\r\n<p><img alt=\"khu-nghi-duong-new-world-phu-quoc-ivivu-2\" class=\"aligncenter size-full wp-image-352574\" src=\"https://cdn3.ivivu.com/2021/10/khu-nghi-duong-new-world-phu-quoc-ivivu-2.jpg\" style=\"height:800px; width:1200px\" /> <img alt=\"khu-nghi-duong-new-world-phu-quoc-ivivu-3\" class=\"aligncenter size-full wp-image-352575\" src=\"https://cdn3.ivivu.com/2021/10/khu-nghi-duong-new-world-phu-quoc-ivivu-3.jpg\" style=\"height:799px; width:1200px\" />Sở hữu 4 nhà hàng và quán bar, resort cũng là một thế giới ẩm thực đầy những bất ngờ. Bay Kitchen phục vụ đặc sản địa phương và ẩm thực Âu, được chế biến và biểu diễn ngay tại khu bếp mở để thực khách chiêm ngưỡng. Nhà hàng Lửa gây ấn tượng với thiết kế lấy cảm hứng từ những làng chài trên đảo. Quầy bar The Lounge và Bar bể bơimang đến không gian thư giãn hoàn hảo với cocktail, đồ uống tinh tế.</p>\r\n\r\n<div style=\"height:172px; margin-bottom:15px; margin-left:0px; margin-right:0px; margin-top:15px\"><iframe height=\"147\" scrolling=\"no\" src=\"https://www.ivivu.com/hothotel/?placeslug=phu-quoc&number=1&width=481\" width=\"100%\"></iframe></div>\r\n', '2022-12-05 23:17:03', 3, 1, 'phu-quoc-thay-doi-ra-sao-sau-mot-nam-len-thanh-pho-1640158783.jpg', 1),
(26, 'Những lý do khiến du khách mong Phú Quốc mở cửa du lịch', '<h1>Những l&yacute; do khiến du kh&aacute;ch mong Ph&uacute; Quốc mở cửa du lịch</h1>\r\n\r\n<p>Với cảnh quan đẹp, ẩm thực phong ph&uacute;, nhiều điểm vui chơi l&yacute; th&uacute;, Ph&uacute; Quốc khiến du kh&aacute;ch trong v&agrave; ngo&agrave;i nước mong ng&agrave;y mở cửa trở lại.</p>\r\n\r\n<div class=\"fig-picture\" style=\"padding-bottom:57.851239669421%; position:relative\"><img alt=\"Toàn cảnh The Shells Resort &amp; Spa khi nhìn từ trên cao. Ảnh: The Shells Resort &amp; Spa\" class=\"lazied lazy\" src=\"https://i1-dulich.vnecdn.net/2021/10/19/A1-6028-1634633783.png?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=Zz9-nbhvTNrJPs_bOTPhpQ\" style=\"left:0; position:absolute; width:100%\" /></div>\r\n\r\n<p>To&agrave;n cảnh The Shells Resort &amp; Spa khi nh&igrave;n từ tr&ecirc;n cao. Ảnh: <em>The Shells Resort &amp; Spa</em></p>\r\n\r\n<p><strong>Quy tụ những điểm nghỉ dưỡng h&agrave;ng đầu</strong></p>\r\n\r\n<p>Sở hữu nhiều b&atilde;i tắm đẹp nhất Đ&ocirc;ng Nam &Aacute;, kh&ocirc;ng kh&oacute; hiểu khi Ph&uacute; Quốc l&agrave; nơi quy tụ h&agrave;ng loạt resort sang trọng với kiến tr&uacute;c mới lạ c&ugrave;ng c&ugrave;ng dịch vụ nghỉ dưỡng đẳng cấp quốc tế. Bạn c&oacute; thể dễ d&agrave;ng lựa chọn Salinda Resort Phu Quoc Island với kiến tr&uacute;c đương đại kết hợp c&ugrave;ng văn h&oacute;a bản địa mang đậm phong c&aacute;ch &Aacute; Đ&ocirc;ng. Famiana Resort &amp; Spa c&oacute; phong c&aacute;ch trang nh&atilde;, ấm c&uacute;ng l&agrave; lựa chọn tuyệt vời cho những người y&ecirc;u thi&ecirc;n nhi&ecirc;n. Một lựa chọn kh&aacute;c l&agrave; Saigon Phu Quoc cho những ai lần đầu đặt ch&acirc;n đến đảo Ngọc với đầy đủ dịch vụ đưa đ&oacute;n từ s&acirc;n bay, thu&ecirc; xe miễn ph&iacute;, đặt tour v&agrave; rất nhiều những tiện &iacute;ch kh&aacute;c. Ngo&agrave;i ra, Eden Resort Phu Quoc, Tropicana Resort Phu Quoc, My Place Siena Garden... cũng l&agrave; những c&aacute;i t&ecirc;n c&oacute; thể mang tới một kỳ nghỉ trong mơ.</p>\r\n\r\n<p><strong>Nhiều điểm du lịch hấp dẫn</strong></p>\r\n\r\n<p>Ph&uacute; Quốc nổi tiếng với những điểm tham quan như Safari Ph&uacute; Quốc, chợ đ&ecirc;m, c&aacute;p treo, bảo t&agrave;ng,.... B&ecirc;n cạnh đ&oacute;, đảo Ngọc vẫn c&ograve;n nhiều địa điểm nguy&ecirc;n sơ để du kh&aacute;ch kh&aacute;m ph&aacute;, thỏa th&iacute;ch check in kh&ocirc;ng đụng h&agrave;ng như: Vườn quốc gia Ph&uacute; Quốc, th&aacute;c v&agrave; suối Tranh, h&ograve;n Đồi Mồi, h&ograve;n Một.</p>\r\n\r\n<div class=\"fig-picture\" style=\"padding-bottom:100%; position:relative\"><img alt=\"CaptionPhú Quốc thu hút du khách với nhiều điểm du lịch hấp dẫn. (Ảnh: T. Nguyễn)\" class=\"lazied lazy\" src=\"https://i1-dulich.vnecdn.net/2021/10/19/phu-quoc-2626-1634633783.png?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=CLYzAW2vKKRQpogDfykh-Q\" style=\"left:0; position:absolute; width:100%\" /></div>\r\n\r\n<p>Ph&uacute; Quốc thu h&uacute;t du kh&aacute;ch với nhiều điểm du lịch hấp dẫn. Ảnh: <em>T. Nguyễn</em></p>\r\n\r\n<p><strong>Thi&ecirc;n đường ẩm thực</strong></p>\r\n\r\n<p>N&oacute;i đến ẩm thực Ph&uacute; Quốc l&agrave; n&oacute;i đến những m&oacute;n hải sản tươi ngon kh&oacute; nơi n&agrave;o s&aacute;nh được như: gỏi c&aacute; tr&iacute;ch, rau rừng chấm nước mắm truyền thống trứ danh, ghẹ H&agrave;m Ninh thịt chắc v&agrave; thơm hay mực chớp chấm muối hồng ti&ecirc;u Ph&uacute; Quốc cay nồng... D&ugrave; được phục vụ tại c&aacute;c qu&aacute;n ăn d&acirc;n d&atilde; hay tại c&aacute;c kh&aacute;ch sạn 5 sao, ẩm thực Ph&uacute; Quốc đều mang đến cho du kh&aacute;ch thập phương cảm nhận chung đ&oacute; l&agrave; ăn ngon, gi&aacute; rẻ.</p>\r\n\r\n<div class=\"fig-picture\" style=\"padding-bottom:56.275720164609%; position:relative\"><img alt=\"CaptionẨm thực Phú Quốc đa dạng và tươi ngon. (Ảnh: T. Nguyễn)\" class=\"lazied lazy\" src=\"https://i1-dulich.vnecdn.net/2021/10/19/phu-quoc1-3884-1634633783.png?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=_uf0glhxdUEJmJcv8HeTBw\" style=\"left:0; position:absolute; width:100%\" /></div>\r\n\r\n<p>Ẩm thực Ph&uacute; Quốc đa dạng v&agrave; tươi ngon. Ảnh: <em>T. Nguyễn</em></p>\r\n\r\n<p><strong>Những sự</strong><strong> kiện</strong><strong> ho&agrave;nh tr&aacute;ng</strong></p>\r\n\r\n<p>Thừa hưởng hệ thống kh&aacute;ch sạn v&agrave; resort đẳng cấp từng thu h&uacute;t nhiều ng&ocirc;i sao trong v&agrave; ngo&agrave;i nước lựa chọn l&agrave; địa điểm để tổ chức tiệc, lễ cưới, Ph&uacute; Quốc ng&agrave;y c&agrave;ng trẻ ho&aacute; c&aacute;c hoạt động du lịch với h&agrave;ng loạt show diễn, bữa tiệc &acirc;m nhạc ho&agrave;nh tr&aacute;ng. C&aacute;c điểm đến Sunset Sanato, Skybar &ndash; Inter Continental, Vin Resort, Vin Wonder, Ốc S&ecirc;n Bar thường xuy&ecirc;n thu h&uacute;t giới trẻ t&igrave;m tới tham dự c&aacute;c sự kiện lớn. Tại &quot;th&agrave;nh phố kh&ocirc;ng ngủ&quot; Ph&uacute; Quốc United Center, kh&ocirc;ng kh&iacute; lễ hội đua thuyền, lễ hội carnaval s&ocirc;i động đến từ Th&aacute;i Lan, Ấn Độ, Mỹ, Đức, Mexico... cũng mang đến cho du kh&aacute;ch c&oacute; những trải nghiệm đầy ấn tượng.</p>\r\n', '2022-12-05 23:19:15', 3, 1, 'khu-nghi-duong-new-world-phu-quoc-ivivu-2.jpg', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `binh_luan`
--

CREATE TABLE `binh_luan` (
  `id` int(11) NOT NULL,
  `noi_dung` text NOT NULL,
  `id_nguoi_dung` int(11) NOT NULL,
  `id_bai_viet` int(11) NOT NULL,
  `thoi_gian` datetime NOT NULL DEFAULT current_timestamp(),
  `trang_thai` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `binh_luan`
--

INSERT INTO `binh_luan` (`id`, `noi_dung`, `id_nguoi_dung`, `id_bai_viet`, `thoi_gian`, `trang_thai`) VALUES
(1, 'Bài viết hay quá comment 1 cái nè', 1, 1, '2022-04-01 00:00:00', 1),
(3, 'Bài viết tuyệt vời', 1, 1, '2022-04-01 00:00:00', 1),
(4, 'Tuyệt tác !!!!!', 3, 1, '2022-04-01 00:00:00', 1),
(5, 'Đây là một siêu phẩm', 3, 2, '2022-04-01 00:00:00', 1),
(6, 'Bài viết tuyệt vời', 1, 1, '2022-04-01 21:49:39', 1),
(7, 'Tuyệt phẩm', 1, 2, '2022-04-02 14:19:03', 1),
(25, 'This is demo comment', 3, 1, '2022-04-09 20:45:45', 1),
(26, 'This is demo comment!', 3, 3, '2022-04-09 20:59:46', 1),
(27, '121', 1, 1, '2022-04-11 19:12:59', 1),
(28, 'Tuyệt vời', 3, 7, '2022-04-12 13:49:41', 1),
(29, 'This is demo comment!', 3, 7, '2022-04-12 14:10:14', 1),
(51, 'test noti', 1, 8, '2022-04-21 21:55:32', 1),
(52, 'test noti', 3, 8, '2022-04-21 21:56:08', 1),
(53, 'test noti 2', 1, 8, '2022-04-21 21:59:12', 1),
(54, 'test noti 2', 1, 12, '2022-04-21 21:59:35', 1),
(55, 'a', 1, 6, '2022-04-23 10:39:01', 1),
(56, 'test', 9, 19, '2022-04-23 14:09:09', 1),
(57, 'Đây là 1 bình luận mới', 9, 19, '2022-04-23 14:09:38', 1),
(58, 'Tesr mới', 9, 19, '2022-04-23 14:10:49', 1),
(59, 'test', 3, 19, '2022-04-23 14:11:40', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nguoi_dung`
--

CREATE TABLE `nguoi_dung` (
  `id` int(11) NOT NULL,
  `ten` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sdt` varchar(30) NOT NULL,
  `mo_ta` varchar(255) DEFAULT NULL,
  `phan_quyen` varchar(20) NOT NULL DEFAULT 'user',
  `mat_khau` varchar(50) NOT NULL,
  `anh_dai_dien` varchar(200) CHARACTER SET utf8 DEFAULT 'avatar_default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `nguoi_dung`
--

INSERT INTO `nguoi_dung` (`id`, `ten`, `email`, `sdt`, `mo_ta`, `phan_quyen`, `mat_khau`, `anh_dai_dien`) VALUES
(1, 'Trần Văn Quang', 'quangtrn8821@gmail.com', '0359759523', 'Creator of this social blog', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '1648915713_wallpaperflare.com_wallpaper.jpg'),
(3, 'Đào Gia Bảo', 'daogiabao2001@gmail.com', '0961597282', 'I\'m a PHP developer.', 'user', 'e10adc3949ba59abbe56e057f20f883e', '1649156889_itplus.png'),
(9, 'Nguyễn Thanh Nam', 'haingan011201@gmail.com', '0985576041', 'Test', 'user', '32ee492d4d944c0283aed7eeb7f0c931', '1650697679_nhung-doi-nike-air-jordan-se-duoc-phat-hanh-nam-2022.2-768x548.jpg'),
(10, 'hiiii', 'dambacaiprj2@gmail.com', '0851111111', NULL, 'user', '4297f44b13955235245b2497399d7a93', 'avatar_default.jpg'),
(11, '123456765433456', '123456765433456@gmail.com', '0923456787', NULL, 'user', 'dcf6cf6e9a964f9f8fd541c44c7b5bb8', 'avatar_default.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `quan_ly`
--

CREATE TABLE `quan_ly` (
  `id` int(11) NOT NULL,
  `id_nguoi_dung` int(11) NOT NULL,
  `id_bai_viet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `the_loai`
--

CREATE TABLE `the_loai` (
  `id` int(11) NOT NULL,
  `ten_the_loai` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `the_loai`
--

INSERT INTO `the_loai` (`id`, `ten_the_loai`) VALUES
(1, 'Sapa'),
(2, 'Đà Nẵng'),
(3, 'Phú Quốc'),
(4, 'Nha Trang');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `thong_bao`
--

CREATE TABLE `thong_bao` (
  `id` int(11) NOT NULL,
  `noi_dung` text NOT NULL,
  `thoi_gian` datetime NOT NULL DEFAULT current_timestamp(),
  `id_bai_viet` int(11) NOT NULL,
  `id_nguoi_comment` int(11) NOT NULL,
  `id_tac_gia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `thong_bao`
--

INSERT INTO `thong_bao` (`id`, `noi_dung`, `thoi_gian`, `id_bai_viet`, `id_nguoi_comment`, `id_tac_gia`) VALUES
(1, 'Đã bình luận bài viết của bạn', '2022-04-21 18:35:12', 1, 1, 3),
(2, 'Đã bình luận bài viết của bạn', '2022-04-21 18:35:12', 2, 3, 1),
(3, 'Đã trả lời bình luận của bạn', '2022-04-21 21:24:17', 1, 1, 3),
(4, 'Đã bình luận bài viết của bạn', '2022-04-21 21:55:32', 8, 1, 1),
(5, 'Đã bình luận bài viết của bạn', '2022-04-21 21:56:08', 8, 3, 1),
(6, 'Đã bình luận bài viết của bạn', '2022-04-21 21:59:13', 8, 1, 1),
(7, 'Đã bình luận bài viết của bạn', '2022-04-21 21:59:35', 12, 1, 3),
(8, 'Đã trả lời bình luận của bạn', '2022-04-21 22:31:35', 7, 1, 3),
(9, 'Đã bình luận bài viết của bạn', '2022-04-23 10:39:01', 6, 1, 1),
(10, 'Đã bình luận bài viết của bạn', '2022-04-23 14:09:09', 19, 9, 9),
(11, 'Đã bình luận bài viết của bạn', '2022-04-23 14:09:38', 19, 9, 9),
(12, 'Đã bình luận bài viết của bạn', '2022-04-23 14:10:49', 19, 9, 9),
(13, 'Đã bình luận bài viết của bạn', '2022-04-23 14:11:40', 19, 3, 9),
(14, 'Đã trả lời bình luận của bạn', '2022-04-23 14:11:59', 19, 3, 9);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tra_loi_binh_luan`
--

CREATE TABLE `tra_loi_binh_luan` (
  `id` int(11) NOT NULL,
  `noi_dung` text CHARACTER SET utf8 NOT NULL,
  `id_binh_luan` int(11) NOT NULL,
  `id_nguoi_dung` int(11) NOT NULL,
  `thoi_gian` datetime NOT NULL DEFAULT current_timestamp(),
  `trang_thai` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tra_loi_binh_luan`
--

INSERT INTO `tra_loi_binh_luan` (`id`, `noi_dung`, `id_binh_luan`, `id_nguoi_dung`, `thoi_gian`, `trang_thai`) VALUES
(1, 'Bình luận tiêu cực ghê quá trời ơi', 1, 1, '2022-04-02 21:59:52', 1),
(2, 'Hay quá nè', 1, 3, '2022-04-02 21:59:52', 1),
(3, 'Trên cả tuyệt vời nữa chứ', 3, 3, '2022-04-02 22:04:56', 1),
(4, '212', 1, 1, '2022-04-11 19:13:06', 1),
(5, 'Cảm ơn bạn', 28, 1, '2022-04-12 13:50:38', 1),
(6, 'This is demo comment too', 29, 1, '2022-04-12 14:11:02', 1),
(7, 'thanks for test!', 54, 3, '2022-04-21 22:01:37', 1),
(8, 'test noti', 29, 1, '2022-04-21 22:31:35', 1),
(9, 'test', 57, 3, '2022-04-23 14:11:59', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `yeu_thich_bai_viet`
--

CREATE TABLE `yeu_thich_bai_viet` (
  `id` int(11) NOT NULL,
  `id_nguoi_dung` int(11) NOT NULL,
  `id_bai_viet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bai_viet`
--
ALTER TABLE `bai_viet`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `binh_luan`
--
ALTER TABLE `binh_luan`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `quan_ly`
--
ALTER TABLE `quan_ly`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `the_loai`
--
ALTER TABLE `the_loai`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `thong_bao`
--
ALTER TABLE `thong_bao`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tra_loi_binh_luan`
--
ALTER TABLE `tra_loi_binh_luan`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `yeu_thich_bai_viet`
--
ALTER TABLE `yeu_thich_bai_viet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bai_viet`
--
ALTER TABLE `bai_viet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `binh_luan`
--
ALTER TABLE `binh_luan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `quan_ly`
--
ALTER TABLE `quan_ly`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `the_loai`
--
ALTER TABLE `the_loai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `thong_bao`
--
ALTER TABLE `thong_bao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `tra_loi_binh_luan`
--
ALTER TABLE `tra_loi_binh_luan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `yeu_thich_bai_viet`
--
ALTER TABLE `yeu_thich_bai_viet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
