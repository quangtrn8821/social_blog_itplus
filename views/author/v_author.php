<!-- Start Author Area  -->

<!-- Start Author Info -->
<div class="axil-author-area axil-author-banner bg-color-grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-author">
                    <div class="media">
                        <?php foreach ($author as $key => $value) { ?>
                            <div class="thumbnail">
                                <a href="#">
                                    <img src="public/image/user/<?php echo $value->anh_dai_dien ?>"
                                         style="width: 200px; height: 200px" " alt="Author Images">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="author-info">
                                    <h1 class="title"><a href="#"><?php echo $value->ten ?></a></h1>
                                    <span class="b3 subtitle">Role: <?php echo $value->phan_quyen ?></span>
                                </div>
                                <div class="content">
                                    <p class="b1 description"><?php echo $value->mo_ta ?></p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Author Info -->

<!-- Start Post List Wrapper  -->
<div class="axil-post-list-area axil-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <!-- Start Author Sidebar -->
            <div class="col-lg-4 col-xl-4 mt_md--40 mt_sm--40">
                <!-- Start Sidebar Area  -->
                <div class="sidebar-inner">
                    <!-- side-bar -->
                    <!-- Start Single Widget  -->
                    <div class="axil-single-widget widget widget_categories mb--30">
                        <ul>
                            <li class="cat-item">
                                <a href="catergory_post.php?id_the_loai=1" class="inner">
                                    <div class="thumbnail">
                                        <img src="public/layout/assets/images/post-images/category-image-01.jpg" alt="">
                                    </div>
                                    <div class="content">
                                        <h5 class="title">Sapa</h5>
                                    </div>
                                </a>
                            </li>
                            <li class="cat-item">
                                <a href="catergory_post.php?id_the_loai=2" class="inner">
                                    <div class="thumbnail">
                                        <img src="public/layout/assets/images/post-images/category-image-02.jpg" alt="">
                                    </div>
                                    <div class="content">
                                        <h5 class="title">Đà Nẵng</h5>
                                    </div>
                                </a>
                            </li>
                            <li class="cat-item">
                                <a href="catergory_post.php?id_the_loai=4" class="inner">
                                    <div class="thumbnail">
                                        <img src="public/layout/assets/images/post-images/category-image-03.jpg" alt="">
                                    </div>
                                    <div class="content">
                                        <h5 class="title">Nha Trang</h5>
                                    </div>
                                </a>
                            </li>
                            <li class="cat-item">
                                <a href="catergory_post.php?id_the_loai=3" class="inner">
                                    <div class="thumbnail">
                                        <img src="public/layout/assets/images/post-images/category-image-04.jpg" alt="">
                                    </div>
                                    <div class="content">
                                        <h5 class="title">Phú Quốc</h5>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Single Widget  -->

                    <!-- phổ biến -->
                    <!-- Start Single Widget  -->
                    <div class="axil-single-widget widget widget_postlist mb--30">
                        <h5 class="widget-title">Phổ biến</h5>
                        <!-- Start Post List  -->
                        <div class="post-medium-block">

                            <!-- Start Single Post  -->
                            <div class="content-block post-medium mb--20">
                                <div class="post-thumbnail">
                                    <a href="post-details.html">
                                        <img src="public/layout/assets/images/small-images/blog-sm-01.jpg"
                                             alt="Post Images">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <h6 class="title"><a href="post-details.html">The underrated design book that
                                            transformed the way I
                                            work </a></h6>
                                    <div class="post-meta">
                                        <ul class="post-meta-list">
                                            <li>Feb 17, 2019</li>
                                            <li>300k Views</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Post  -->

                            <!-- Start Single Post  -->
                            <div class="content-block post-medium mb--20">
                                <div class="post-thumbnail">
                                    <a href="post-details.html">
                                        <img src="public/layout/assets/images/small-images/blog-sm-02.jpg"
                                             alt="Post Images">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <h6 class="title"><a href="post-details.html">Here’s what you should (and shouldn’t)
                                            do when</a>
                                    </h6>
                                    <div class="post-meta">
                                        <ul class="post-meta-list">
                                            <li>Feb 17, 2019</li>
                                            <li>300k Views</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Post  -->

                            <!-- Start Single Post  -->
                            <div class="content-block post-medium mb--20">
                                <div class="post-thumbnail">
                                    <a href="post-details.html">
                                        <img src="public/layout/assets/images/small-images/blog-sm-03.jpg"
                                             alt="Post Images">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <h6 class="title"><a href="post-details.html">How a developer and designer duo at
                                            Deutsche Bank keep
                                            remote</a></h6>
                                    <div class="post-meta">
                                        <ul class="post-meta-list">
                                            <li>Feb 17, 2019</li>
                                            <li>300k Views</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Post  -->

                        </div>
                        <!-- End Post List  -->

                    </div>
                    <!-- End Single Widget  -->
                </div>
                <!-- End Sidebar Area  -->
            </div>
            <!-- End Author Sidebar -->

            <!-- Start Author Post List -->
            <!-- danh sách bài viết của user -->
            <div class="col-lg-8">
                <div class="page-title">
                    <h2 class="title mb--40">Danh sách bài viết</h2>
                </div>
                <!-- Start Axil Tab Button  -->
                <ul class="axil-tab-button nav nav-tabs mt--20" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a
                                class="nav-link active"
                                id="trend-one"
                                data-bs-toggle="tab"
                                href="#baivietcuatoi"
                                role="tab"
                                aria-controls="trend-one"
                                aria-selected="true"
                        >Bài viết của <?php echo $name_author ?></a
                        >
                    </li>
                </ul>
                <!-- End Axil Tab Button  -->

                <!-- Start Axil Tab Content  -->
                <div class="tab-content">
                    <!-- Start Single Tab Content  -->
                    <div
                            class="row trend-tab-content tab-pane fade show active"
                            id="baivietcuatoi"
                            role="tabpanel"
                            aria-labelledby="trend-one"
                    >
                        <div class="col-lg-12">
                            <!-- Start Single Post  -->
                            <div
                                    class="content-block trend-post post-order-list axil-control"
                            >
                                <!-- Start Post List  -->
                                <?php foreach ($user_post_list as $key=>$value) {?>
                                    <div class="content-block post-list-view" style="margin-top: 10px;">
                                        <div class="post-thumbnail">
                                            <a href="post-details.html">
                                                <img src="public/image/post/<?php echo $value->anh_tieu_de ?>">
                                            </a>
                                            <a class="video-popup icon-color-secondary size-medium position-top-center" href="post-details.html"><span class="play-icon"></span></a>
                                        </div>
                                        <div class="post-content">
                                            <div class="post-cat">
                                                <div class="post-cat-list">
                                                    <a class="hover-flip-item-wrapper" href="#">
                                            <span>
                                                <span><?php echo $value->ten_the_loai ?></span>
                                            </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <h4 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id ?>"><?php echo $value->ten_bai_viet ?></a></h4>
                                            <div class="post-meta-wrapper">
                                                <div class="post-meta">
                                                    <div class="content">
                                                        <h6 class="post-author-name">
                                                            <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $value->id ?>">
                                                    <span>
                                                        <span><?php echo $value->ten ?></span>
                                                    </span>
                                                            </a>
                                                        </h6>
                                                        <ul class="post-meta-list">
                                                            <li><?php echo $value->thoi_gian_tao ?></li>
                                                            <li>Số views</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                <?php }?>
                            </div>
                            <!-- End Single Post  -->
                        </div>
                    </div>
                    <!-- End Single Tab Content  -->
                </div>
            <!-- End Author Post List -->
        </div>
    </div>
</div>
<!-- End Post List Wrapper  -->
<!-- End Author Area  -->

