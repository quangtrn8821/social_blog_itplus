<!-- Start Banner Area -->
<h1 class="d-none">Home Default Blog</h1>
<div class="slider-area bg-color-grey">
    <div class="axil-slide slider-style-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="slider-activation axil-slick-arrow">
                        <!-- Start Single Slide  -->
                        <div>
                            <?php foreach ($technology as $key =>$value){?>
                            <div class="content-block">
                                <!-- Start Post Thumbnail  -->
                                <div class="post-thumbnail">
                                    <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                                        <img style="width:2880px; height: 550px " src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                                    </a>
                                </div>
                                <!-- End Post Thumbnail  -->

                                <!-- Start Post Content  -->
                                <div class="post-content">
                                    <div class="post-cat">
                                        <div class="post-cat-list">
                                            <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                                    <span class="hover-flip-item">
                                                        <span data-text="<?php echo $value -> ten_the_loai;?>"><?php echo $value -> ten_the_loai;?></span>
                                                    </span>
                                            </a>
                                        </div>
                                    </div>
                                    <h2 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h2>
                                    <!-- Post Meta  -->
                                    <div class="post-meta-wrapper with-button">
                                        <div class="post-meta">
                                            <div class="content">
                                                <h6 class="post-author-name">
                                                    <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $value->id ?>">
                                                            <span class="hover-flip-item">
                                                                <span data-text="<?php echo $value->ten;?>"><?php echo $value->ten;?></span>
                                                            </span>
                                                    </a>
                                                </h6>
                                                <ul class="post-meta-list">
                                                    <li><?php echo $value -> thoi_gian_tao;?></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="read-more-button cerchio">
                                            <a class="axil-button button-rounded hover-flip-item-wrapper" href="post_detail.php?id_bai_viet=<?php echo $value->id;?>">
                                                    <span class="hover-flip-item">
                                                        <span data-text="Đọc bài viết">Đọc bài viết</span>
                                                    </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Post Content  -->
                            </div>
                            <?php if( $value ->ten);}?>
                        </div>
                        <!-- End Single Slide  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Banner Area -->

<!-- Start Featured Area  -->
<div class="axil-featured-post axil-section-gap bg-color-grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2 class="title">Sapa</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($technology2 as $key =>$value){?>
            <!-- Start Single Post  -->
            <div class="col-lg-6 col-xl-6 col-md-12 col-12 mt--30">
                <div class="content-block content-direction-column axil-control is-active post-horizontal thumb-border-rounded">

                    <div class="post-content">
                        <div class="post-cat">
                            <div class="post-cat-list">
                                <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                            <span class="hover-flip-item">
                                                <span data-text="<?php echo $value -> ten_the_loai;?>"><?php echo $value -> ten_the_loai;?></span>
                                            </span>
                                </a>
                            </div>
                        </div>
                        <h4 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h4>
                        <div class="post-meta">
                            <div class="content">
                                <h6 class="post-author-name">
                                    <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $value->id ?>">
                                                <span class="hover-flip-item">
                                                   <span data-text="<?php echo $value->ten;?>"><?php echo $value->ten;?></span>
                                                </span>
                                    </a>
                                </h6>
                                <ul class="post-meta-list">
                                    <li><?php echo $value -> thoi_gian_tao;?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="post-thumbnail">
                        <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                            <img src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                        </a>
                    </div>

                </div>
            </div>
            <!-- End Single Post  -->
                <?php if($value -> thoi_gian_tao);}?>
        </div>
    </div>
</div>
<!-- End Featured Area  -->
<!-- Start Post Grid Area  -->
<div class="axil-post-grid-area axil-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2 class="title">Phú Quốc</h2>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="grid-tab-content tab-content mt--10">

                    <!-- Start Single Tab Content  -->
                    <div class="single-post-grid tab-pane fade show active" id="gridone" role="tabpanel">
                        <div class="row">
                            <div class="col-xl-7 col-lg-7 col-md-12 col-12">
                                <!-- Start Post Grid  -->
                                <?php foreach ($culinary as $key =>$value){?>
                                <div class="content-block post-grid post-grid-large mt--30">
                                    <div class="post-thumbnail">
                                        <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                                            <img style="height: 660px; width:705px;" src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                                        </a>
                                    </div>
                                    <div class="post-grid-content">
                                        <div class="post-content">
                                            <div class="post-cat">
                                                <div class="post-cat-list">
                                                    <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                                                <span class="hover-flip-item">
                                                                   <span data-text="<?php echo $value -> ten_the_loai;?>"><?php echo $value -> ten_the_loai;?></span>
                                                                </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <h3 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h3>
                                            <div class="post-meta-wrapper">
                                                <div class="post-meta">
                                                    <div class="content">
                                                        <h6 class="post-author-name">
                                                            <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $value->id ?>">
                                                                        <span class="hover-flip-item">
                                                                           <span data-text="<?php echo $value->ten;?>"><?php echo $value->ten;?></span>
                                                                        </span>
                                                            </a>
                                                        </h6>
                                                        <ul class="post-meta-list">
                                                            <li><?php echo $value -> thoi_gian_tao;?></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <!-- Start Post Grid  -->
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-12 col-12">
                                <div class="row">
                                    <?php foreach ($culinary2 as $key =>$value){?>
                                    <div class="col-xl-12 col-lg-12 col-md-6 col-12">
                                        <!-- Start Post Grid  -->
                                        <div class="content-block post-grid mt--30">
                                            <div class="post-thumbnail">
                                                <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                                                    <img src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                                                </a>
                                            </div>
                                            <div class="post-grid-content">
                                                <div class="post-content">
                                                    <div class="post-cat">
                                                        <div class="post-cat-list">
                                                            <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                                                        <span class="hover-flip-item">
                                                                            <span data-text="<?php echo $value -> ten_the_loai;?>"><?php echo $value -> ten_the_loai;?></span>
                                                                        </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h4 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Start Post Grid  -->
                                    </div>
                                    <?php if($value -> thoi_gian_tao);}?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Tab Content  -->
                </div>
                <!-- End Tab Content  -->
            </div>
        </div>
    </div>
</div>
<div class="axil-seo-post-banner seoblog-banner axil-section-gap bg-color-grey">
    <div class="container">
        <div class="col-lg-12">
            <div class="section-title">
                <h2 class="title">Nha Trang</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-12 col-12">
                <!-- Start Post Grid  -->
                <?php foreach ($travel as $key => $value) {?>
                <div class="content-block post-grid post-grid-large">
                    <div class="post-thumbnail">
                        <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                            <img src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                        </a>
                    </div>
                    <div class="post-grid-content">
                        <div class="post-content">
                            <div class="post-cat">
                                <div class="post-cat-list">
                                    <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                                <span class="hover-flip-item">
                                                   <span data-text="<?php echo $value->ten_the_loai; ?>"><?php echo $value->ten_the_loai; ?></span>
                                                </span>
                                    </a>
                                </div>
                            </div>
                            <h3 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h3>
                            <div class="post-meta-wrapper">
                                <div class="post-meta">
                                    <div class="content">
                                        <h6 class="post-author-name">
                                            <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $value->id ?>">
                                                        <span class="hover-flip-item">
                                                            <span data-text="<?php echo $value->ten;?>"><?php echo $value->ten;?></span>
                                                        </span>
                                            </a>
                                        </h6>
                                        <ul class="post-meta-list">
                                            <li><?php echo $value -> thoi_gian_tao;?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
                <!-- Start Post Grid  -->
            </div>
            <div class="col-xl-5 col-lg-5 col-md-12 col-12 mt_md--30 mt_sm--30">
                <!-- Start Single Post  -->
                <?php foreach ($travel2 as $key => $value) {?>
                <div class="content-block post-medium post-medium-border">
                    <div class="post-thumbnail">
                        <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                            <img src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                        </a>
                    </div>
                    <div class="post-content">
                        <div class="post-cat">
                            <div class="post-cat-list">
                                <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                            <span class="hover-flip-item">
                                                 <span data-text="<?php echo $value->ten_the_loai; ?>"><?php echo $value->ten_the_loai; ?></span>
                                            </span>
                                </a>
                            </div>
                        </div>
                        <h4 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h4>
                    </div>
                </div>
                <!-- End Single Post  -->
                <?php }?>
            </div>
        </div>
    </div>
</div>
<div class="axil-tech-post-banner pt--30 bg-color-white">
    <div class="container">
        <div class="col-lg-12">
            <div class="section-title">
                <h2 class="title">Đà Nẵng</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-md-12 col-12">
                <!-- Start Post Grid  -->
                <?php foreach ($fashion as $key => $value){?>
                <div class="content-block post-grid post-grid-transparent">
                    <div class="post-thumbnail">
                        <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                            <img style="width: 600px; height: 600px" src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                        </a>
                    </div>
                    <div class="post-grid-content">
                        <div class="post-content">
                            <div class="post-cat">
                                <div class="post-cat-list">
                                    <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                                <span class="hover-flip-item">
                                                     <span data-text="<?php echo $value->ten_the_loai; ?>"><?php echo $value->ten_the_loai; ?></span>
                                                </span>
                                    </a>
                                </div>
                            </div>
                            <h3 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h3>
                        </div>
                    </div>
                </div>
                <?php if($value -> thoi_gian_tao); }?>
                <!-- Start Post Grid  -->
            </div>

            <div class="col-xl-3 col-md-6 mt_lg--30 mt_md--30 mt_sm--30 col-12">
                <!-- Start Single Post  -->
                <?php foreach ($fashion2 as $key => $value){?>
                <div class="content-block image-rounded">
                    <div class="post-thumbnail">
                        <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                            <img src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                        </a>
                    </div>
                    <div class="post-content pt--20">
                        <div class="post-cat">
                            <div class="post-cat-list">
                                <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                            <span class="hover-flip-item">
                                               <span data-text="<?php echo $value->ten_the_loai; ?>"><?php echo $value->ten_the_loai; ?></span>
                                            </span>
                                </a>
                            </div>
                        </div>
                        <h5 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h5>
                    </div>
                </div>
                <?php }?>
                <!-- End Single Post  -->
            </div>

            <div class="col-xl-3 col-md-6 mt_lg--30 mt_md--30 mt_sm--30 col-12">
                <!-- Start Single Post  -->
                <?php foreach ($fashion3 as $key => $value){?>
                    <div class="content-block image-rounded">
                        <div class="post-thumbnail">
                            <a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>">
                                <img src="public/image/post/post1.jpg" alt="Post Images">
                            </a>
                        </div>
                        <div class="post-content pt--20">
                            <div class="post-cat">
                                <div class="post-cat-list">
                                    <a class="hover-flip-item-wrapper" href="catergory_post.php?id_the_loai=<?php echo $value -> id_the_loai;?>">
                                            <span class="hover-flip-item">
                                               <span data-text="<?php echo $value->ten_the_loai; ?>"><?php echo $value->ten_the_loai; ?></span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                            <h5 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id; ?>"><?php echo $value -> ten_bai_viet;?></a></h5>
                        </div>
                    </div>
                <?php }?>
                <!-- End Single Post  -->
            </div>
        </div>
    </div>
</div>

