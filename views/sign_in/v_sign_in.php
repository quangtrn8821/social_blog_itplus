<body>
<div class="main-wrapper">
        <div class="container login-container">
            <div style="float: left;">
                <div class="content">
                    <div class="logo" >
                        <a href="index.php">
                            <img class="dark-logo" src="public/layout/assets/images/logo/logo.jpg"
                                 alt="Logo Images" style="width: 20%; height: 20%;">
                        </a>
                    </div>
                    <h3 class="title mt--20">ĐĂNG NHẬP</h3>

                    <form id="form_sign_in" method="POST">
                        <div class="form-group" style="width: 80%;">
                            <input type="text" name="email" placeholder="Nhập email của bạn" required>
                            <input type="password" name="password" placeholder="Nhập mật khẩu của bạn"
                                   style="margin-top: 20px;" required>
                            <button style="margin-top: 20px;" class="axil-button button-rounded" type="submit" name="sign_in" value="SignIn">Đăng nhập</button>
                            <center><a href="sign_up.php" >Đăng kí</a></center>
                        </div>
                    </form>
                </div>
            </div>
            <div style="float: right; width: 40%; height: 40%;">
                <div class="thumbnail">
                    <img src="public/layout/assets/images/others/maintenence.png" alt="Images">
                </div>
            </div>
        </div>
</div>

