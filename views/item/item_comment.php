<!-- Start Single Comment  -->
<li class="comment">
    <div class="comment-body">
        <div class="single-comment">
            <div class="comment-img">
                <img src="public/image/user/<?php echo $_POST['anh'] ?>" style="width: 50px; height: 50px;">
            </div>
            <div class="comment-inner">
                <h6 class="commenter">
                    <a class="hover-flip-item-wrapper">
                                    <span class="hover-flip-item">
                                        <span data-text="<?php echo $_POST['ten'] ?>"><?php echo $_POST['ten']; ?></span>
                                    </span>
                    </a>
                </h6>
                <div class="comment-meta">
                    <div class="time-spent"><?php echo $_POST['thoi_gian']; ?></div>
                    <div class="reply-edit">
                        <div class="reply">
                            <a class="comment-reply-link hover-flip-item-wrapper">
                                <span class="hover-flip-item">
                                    <span data-text="Trả lời" class="tra_loi" onclick="showReplyComment(<?php echo $_POST['id_binh_luan'];  ?>)">Trả lời</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="comment-text">
                    <p class="b2"><?php echo $_POST['noi_dung'] ?> </p>
                </div>
            </div>
        </div>
    </div>
<!--    --><?php
//    include_once("controllers/c_reply_comment.php");
//    $c_reply_comment = new c_reply_comment();
//    $c_reply_comment->index($value->id);
//    ?>

</li>
<!-- End Single Comment  -->
