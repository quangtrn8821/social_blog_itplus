<?php include_once ("views/user/v_user_info.php");?>
<!-- Start Post List Wrapper  -->
<div class="axil-post-list-area axil-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <?php include_once ("views/user/v_user_action.php");?>

            <?php include_once ("views/user/v_user_post_list.php");?>
        </div>
    </div>
</div>
<!-- End Post List Wrapper  -->
