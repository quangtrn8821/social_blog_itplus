<div class="col-lg-4 col-xl-4 mt_md--40 mt_sm--40">
    <!-- Start Sidebar Area  -->
    <div class="sidebar-inner">

        <!-- thêm bài viết mới -->
        <!-- Start Single Widget  -->
        <div class="axil-single-widget widget widget_social mb--30">
            <!-- Start Post List  -->
            <a href="create_post.php"><button class="axil-button button-rounded" style="background-color: #2ecc71; border: #2ecc71;">Thêm bài viết</button></a>
            <!-- End Post List  -->
        </div>
        <!-- End Single Widget  -->


        <!-- side-bar -->
        <!-- Start Single Widget  -->
        <div class="axil-single-widget widget widget_categories mb--30">
            <ul>
                <li class="cat-item">
                    <a href="catergory_post.php?id_the_loai=1" class="inner">
                        <div class="thumbnail">
                            <img src="public/layout/assets/images/post-images/category-image-01.jpg" alt="">
                        </div>
                        <div class="content">
                            <h5 class="title">Sapa</h5>
                        </div>
                    </a>
                </li>
                <li class="cat-item">
                    <a href="catergory_post.php?id_the_loai=2" class="inner">
                        <div class="thumbnail">
                            <img src="public/layout/assets/images/post-images/category-image-02.jpg" alt="">
                        </div>
                        <div class="content">
                            <h5 class="title">Đà Nẵng</h5>
                        </div>
                    </a>
                </li>
                <li class="cat-item">
                    <a href="catergory_post.php?id_the_loai=4" class="inner">
                        <div class="thumbnail">
                            <img src="public/layout/assets/images/post-images/category-image-03.jpg" alt="">
                        </div>
                        <div class="content">
                            <h5 class="title">Nha Trang</h5>
                        </div>
                    </a>
                </li>
                <li class="cat-item">
                    <a href="catergory_post.php?id_the_loai=3" class="inner">
                        <div class="thumbnail">
                            <img src="public/layout/assets/images/post-images/category-image-04.jpg" alt="">
                        </div>
                        <div class="content">
                            <h5 class="title">Phú Quốc</h5>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Single Widget  -->

        <!-- phổ biến -->
        <!-- Start Single Widget  -->
        <div class="axil-single-widget widget widget_postlist mb--30">
            <h5 class="widget-title">Phổ biến</h5>
            <!-- Start Post List  -->
            <div class="post-medium-block">

                <!-- Start Single Post  -->
                <div class="content-block post-medium mb--20">
                    <div class="post-thumbnail">
                        <a href="post-details.html">
                            <img src="public/layout/assets/images/small-images/blog-sm-01.jpg" alt="Post Images">
                        </a>
                    </div>
                    <div class="post-content">
                        <h6 class="title"><a href="post-details.html">The underrated design book that transformed the way I
                                work </a></h6>
                        <div class="post-meta">
                            <ul class="post-meta-list">
                                <li>Feb 17, 2019</li>
                                <li>300k Views</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Post  -->

                <!-- Start Single Post  -->
                <div class="content-block post-medium mb--20">
                    <div class="post-thumbnail">
                        <a href="post-details.html">
                            <img src="public/layout/assets/images/small-images/blog-sm-02.jpg" alt="Post Images">
                        </a>
                    </div>
                    <div class="post-content">
                        <h6 class="title"><a href="post-details.html">Here’s what you should (and shouldn’t) do when</a>
                        </h6>
                        <div class="post-meta">
                            <ul class="post-meta-list">
                                <li>Feb 17, 2019</li>
                                <li>300k Views</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Post  -->

                <!-- Start Single Post  -->
                <div class="content-block post-medium mb--20">
                    <div class="post-thumbnail">
                        <a href="post-details.html">
                            <img src="public/layout/assets/images/small-images/blog-sm-03.jpg" alt="Post Images">
                        </a>
                    </div>
                    <div class="post-content">
                        <h6 class="title"><a href="post-details.html">How a developer and designer duo at Deutsche Bank keep
                                remote</a></h6>
                        <div class="post-meta">
                            <ul class="post-meta-list">
                                <li>Feb 17, 2019</li>
                                <li>300k Views</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Post  -->

            </div>
            <!-- End Post List  -->

        </div>
        <!-- End Single Widget  -->

        <!-- ô đăng xuất -->
        <!-- Start Single Widget  -->
        <div class="axil-single-widget widget widget_social mb--30">
            <h5 class="widget-title">Hành động</h5>
            <!-- Start Post List  -->
            <button class="axil-button button-rounded" style="background-color: #e74c3c; border: #e74c3c;">Đăng xuất</button>
            <!-- End Post List  -->
        </div>
        <!-- End Single Widget  -->

    </div>
    <!-- End Sidebar Area  -->
</div>
