<!-- Start Post List Wrapper  -->
<div class="axil-post-list-area axil-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-8">
                <!-- Start About Area  -->
                <div class="axil-about-us">
                    <!-- Start Contact Form  -->
                    <div class="axil-contact-form-area">
                        <h3 class="title mb--10">Cập nhật hồ sơ</h3>
                        <p class="b3 mb--30">Cập nhật thông tin cá nhân của bạn</p>
                        <form id="form_edit_user"
                              class="contact-form--1 row"
                              enctype="multipart/form-data"
                              action="edit_user.php"
                              method="POST">
                            <?php foreach ($user as $key => $value) { ?>
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <label>Họ và tên</label>
                                        <input type="text" name="ten" id="txtHoten" value="<?php echo $value->ten ?>"
                                               required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" id="txtEmail"
                                               value="<?php echo $value->email ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <label>Số điện thoại</label>
                                        <input type="text" name="sdt" id="txtSdt" value="<?php echo $value->sdt ?>"
                                               required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Ảnh đại diện (không cần chọn file nếu không cập nhật avatar)</label>
                                        <input type="file" name="anh_dai_dien" id="fileAvatar" class="form-control"
                                               style="height: 35px; padding: 5px 10px; color: #3858F6;"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="contact-message">Mô tả</label>
                                        <textarea name="mo_ta" id="txtMota"><?php echo $value->mo_ta ?></textarea>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-12">
                                <div class="form-submit">
                                    <button type="submit" name="submit" id="submit" value="Update"
                                            class="axil-button button-rounded btn-primary">Cập nhật
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Contact Form  -->
                </div>
                <!-- End About Area  -->
            </div>
            <?php include_once("views/side_bar/v_side_bar.php"); ?>
        </div>
    </div>
</div>
<!-- End Post List Wrapper  -->
<script type="text/javascript">
    $("#form_edit_user").validate({
        rules: {
            ten: "required",
            email: {
                required: true,
                email: true
            },
            sdt: {
                required: true,
                number: true,
                rangelength: [10, 10],
                phoneVN: true
            },
            anh_dai_dien: {
                extension: "jpg|png|jpeg"
            }
        },
        messages: {
            ten: "Yêu cầu nhập họ tên",
            email: {
                required: "Yêu cầu nhập email",
                email: "Email chưa đúng định dạng"
            },
            sdt: {
                required: "Yêu cầu nhập số điện thoại",
                number: "Số điện thoại chỉ chứa số",
                rangelength: "Độ dài số điện thoại là 10 kí tự"
            },
            anh_dai_dien: {
                extension: "chỉ chấp nhận file có đuôi jpg, png, jpeg"
            }
        }
    });

    jQuery.validator.addMethod("phoneVN", function (value, element) {
        return this.optional(element) || /((09|03|07|08|05)+([0-9]{8})\b)/g.test(value);
    }, "Yêu cầu nhập số điện thoại Việt Nam");
</script>