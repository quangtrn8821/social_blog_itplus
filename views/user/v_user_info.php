<!-- Start Author Area  -->
<div class="axil-author-area axil-author-banner bg-color-grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-author">
                    <div class="media">
                        <?php foreach ($user as $key=>$value) { ?>
                        <div class="thumbnail">
                            <a href="#">
                                <img src="public/image/user/<?php echo $value->anh_dai_dien ?>" style="width: 200px; height: 200px" " alt="Author Images">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="author-info">
                                <h1 class="title"><a href="#"><?php echo $value->ten ?></a></h1>
                                <span class="b3 subtitle">Role: <?php echo $value->phan_quyen ?></span>
                            </div>
                            <div class="content">
                                <p class="b1 description"><?php echo $value->mo_ta ?></p>
                            </div>
                            <div>
                                <a href="edit_user.php"><button type="submit" style="margin-top: 50px; width: 200px" class="axil-button button-rounded color-success center">Cập nhật hồ sơ</button></a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Author Area  -->
