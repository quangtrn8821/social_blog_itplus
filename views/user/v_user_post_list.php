
<!-- danh sách bài viết của user -->
<div class="col-lg-8">
    <div class="page-title">
        <h2 class="title mb--40">Danh sách bài viết</h2>
    </div>
    <!-- Start Axil Tab Button  -->
    <ul class="axil-tab-button nav nav-tabs mt--20" role="tablist">
        <li class="nav-item" role="presentation">
            <a
                    class="nav-link active"
                    id="trend-one"
                    data-bs-toggle="tab"
                    href="#baivietcuatoi"
                    role="tab"
                    aria-controls="trend-one"
                    aria-selected="true"
            >Bài viết của tôi</a
            >
        </li>
        <li class="nav-item" role="presentation"
            <?php if ($_SESSION["role"] == "admin") {
                $display_manage_post = "block";
            } else {
                $display_manage_post = "none";
            }?>
            style="display: <?php echo $display_manage_post ?>">
            <a
                    class="nav-link"
                    id="trend-two"
                    data-bs-toggle="tab"
                    href="#quanlybaiviet"
                    role="tab"
                    aria-controls="trend-two"
                    aria-selected="false"
            >Quản lý bài viết</a
            >
        </li>
    </ul>
    <!-- End Axil Tab Button  -->

    <!-- Start Axil Tab Content  -->
    <div class="tab-content">
        <!-- Start Single Tab Content  -->
        <div
                class="row trend-tab-content tab-pane fade show active"
                id="baivietcuatoi"
                role="tabpanel"
                aria-labelledby="trend-one"
        >
            <div class="col-lg-12">
                <!-- Start Single Post  -->
                <div
                        class="content-block trend-post post-order-list axil-control"
                >
                    <!-- Start Post List  -->
                    <?php foreach ($user_post_list as $key=>$value) {?>
                        <div class="content-block post-list-view" style="margin-top: 10px;">
                            <div class="post-thumbnail">
                                <a href="post-details.html">
                                    <img src="public/image/post/<?php echo $value->anh_tieu_de ?>">
                                </a>
                                <a class="video-popup icon-color-secondary size-medium position-top-center" href="post-details.html"><span class="play-icon"></span></a>
                            </div>
                            <div class="post-content">
                                <div class="post-cat">
                                    <div class="post-cat-list">
                                        <a class="hover-flip-item-wrapper" href="#">
                                            <span>
                                                <span><?php echo $value->ten_the_loai ?></span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <h4 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id ?>"><?php echo $value->ten_bai_viet ?></a></h4>
                                <div class="post-meta-wrapper">
                                    <div class="post-meta">
                                        <div class="content">
                                            <h6 class="post-author-name">
                                                <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $value->id ?>">
                                                    <span>
                                                        <span><?php echo $value->ten ?></span>
                                                    </span>
                                                </a>
                                            </h6>
                                            <ul class="post-meta-list">
                                                <li><?php echo $value->thoi_gian_tao ?></li>
                                                <li>Số views</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul class="social-share-transparent justify-content-end">
                                        <form action="edit_post.php" method="POST">
                                            <input type="hidden" name="id_bai_viet" value="<?php echo $value->id ?>">
                                            <li><button type="submit" class="axil-button button-rounded" style="padding: 0px 30px; background-color: #2ecc71; border: #2ecc71; ">Sửa</button></li>
                                        </form>
                                        <form action="#" method="POST">
                                            <input type="hidden" name="id_bai_viet" value="<?php echo $value->id ?>">
                                            <input type="hidden" name="email" value="<?php echo $value->email ?>">
                                            <li><button type="button" name="submit" value="<?php echo $value->id ?>" class="axil-button button-rounded btn-delete" style="padding: 0px 30px; background-color: #e74c3c; border: #e74c3c;">Xóa</button></li>
                                        </form>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    <?php }?>
                </div>
                <!-- End Single Post  -->
            </div>
        </div>
        <!-- End Single Tab Content  -->

        <!-- Start Single Tab Content  -->
        <div
                class="row trend-tab-content tab-pane fade show"
                id="quanlybaiviet"
                role="tabpanel"
                aria-labelledby="trend-two"
        >
            <div class="col-lg-12">
                <!-- Start Single Post  -->
                <div
                        class="content-block trend-post post-order-list axil-control"
                >
                    <!-- Start Post List  -->
                    <?php foreach ($all_post_list as $key=>$value) {?>
                        <div class="content-block post-list-view" style="margin-top: 10px;">
                            <div class="post-thumbnail">
                                <a href="post-details.html">
                                    <img src="public/image/post/<?php echo $value->anh_tieu_de ?>">
                                </a>
                                <a class="video-popup icon-color-secondary size-medium position-top-center" href="post-details.html"><span class="play-icon"></span></a>
                            </div>
                            <div class="post-content">
                                <div class="post-cat">
                                    <div class="post-cat-list">
                                        <a class="hover-flip-item-wrapper" href="#">
                                            <span>
                                                <span><?php echo $value->ten_the_loai ?></span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <h4 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id ?>"><?php echo $value->ten_bai_viet ?></a></h4>
                                <div class="post-meta-wrapper">
                                    <div class="post-meta">
                                        <div class="content">
                                            <h6 class="post-author-name">
                                                <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $value->id ?>">
                                                    <span>
                                                        <span><?php echo $value->ten ?></span>
                                                    </span>
                                                </a>
                                            </h6>
                                            <ul class="post-meta-list">
                                                <li><?php echo $value->thoi_gian_tao ?></li>
                                                <li>Số views</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul class="social-share-transparent justify-content-end">
<!--                                        <li><button class="axil-button button-rounded" style="padding: 0px 30px; background-color: #2ecc71; border: #2ecc71; ">Sửa</button></li>-->
                                        <form action="#" method="POST">
                                            <li><button type="button" name="submit" value="<?php echo $value->id ?>" class="axil-button button-rounded btn-delete" style="padding: 0px 30px; background-color: #e74c3c; border: #e74c3c;">Xóa</button></li>
                                        </form>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    <?php }?>
                </div>
                <!-- End Single Post  -->
            </div>
        </div>
        <!-- End Single Tab Content  -->
    </div>

    <script type="text/javascript">
        $(".btn-delete").click(function () {
            var id = $(this).val();
            Swal.fire({
                title: 'Xác nhận',
                text: "Bạn chắc chắn xóa bài viết này?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Xóa',
                cancelButtonText: 'Hủy'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.post("delete_post.php",
                        {
                            id_bai_viet: id
                        },
                        (data, status) => {});
                    window.location="user.php";
                }
            })
        })
    </script>