<body>
<div class="main-wrapper">
    <div class="container login-container">
            <div>
                <div class="thumbnail">
                    <img src="public/layout/assets/images/others/bg_sign_up.jpg" alt="Images">
                </div>
            </div>
            <div>
                <div class="content">
                    <div class="logo">
                        <a href="index.php">
                            <img class="dark-logo" src="public/layout/assets/images/logo/logo.jpg"
                                 alt="Logo Images" style="width: 20%; height: 20%;">
                        </a>
                    </div>
                    <h3 class="title mt--20">ĐĂNG KÍ</h3>
                    <form id="form_sign_up" method="POST">
                        <div class="form-group" style="width: 80%;">
                            <input type="email" name="email" placeholder="Nhập email" required>
                            <input type="text" name="sdt" placeholder="Nhập số điện thoại"
                                   style="margin-top: 20px;" required>
                            <input type="text" name="username" placeholder="Nhập họ tên"
                                   style="margin-top: 20px;" required>
                            <input type="password" id="password" name="password" placeholder="Nhập mật khẩu"
                                   style="margin-top: 20px;" required>
                            <input type="password" id="confirm_password" name="confirm_password" placeholder="Nhập lại mật khẩu"
                                   style="margin-top: 20px;" required>
                            <button type="submit" name="sign_up" value="SignUp" style="margin-top: 20px;"
                                    class="axil-button button-rounded">Đăng ký
                            </button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>

<script type="text/javascript">
    $("#form_sign_up").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            sdt: {
                required: true,
                number: true,
                rangelength: [10,10],
                phoneVN: true
            },
            username: "required",
            password: "required",
            confirm_password: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            email: {
                required: "Yêu cầu nhập email",
                email: "Email chưa đúng định dạng"
            },
            sdt: {
                required: "Yêu cầu nhập số điện thoại",
                number: "Số điện thoại chỉ chứa số",
                rangelength: "Độ dài số điện thoại là 10 kí tự"
            },
            username: "Yêu cầu nhập họ tên",
            password: "Yêu cầu nhập mật khẩu",
            confirm_password: {
                required: "Yêu cầu xác nhận mật khẩu",
                equalTo: "Mật khẩu xác nhận phải trùng với mật khẩu"
            }
        }
    });

    jQuery.validator.addMethod("phoneVN", function(value, element) {
        return this.optional(element) || /((09|03|07|08|05)+([0-9]{8})\b)/g.test(value);
    }, "Yêu cầu nhập số điện thoại Việt Nam");
</script>
