<!-- Start Banner Area -->
<div class="post-single-wrapper axil-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- Start Banner Area -->
                <div class="banner banner-single-post post-formate post-layout axil-section-gapBottom">
                    <div class="container">
                        <div class="row">
                            <?php foreach ($post as $key => $value){ ?>
                            <div class="col-lg-12">
                                <!-- Start Single Slide  -->
                                <div class="content-block">
                                    <!-- Start Post Content  -->
                                    <div class="post-content">
                                        <div class="post-cat">
                                            <div class="post-cat-list">
                                                <a class="hover-flip-item-wrapper" href="#">
                                                            <span class="hover-flip-item">
                                                                <span data-text="<?php echo $value -> ten_the_loai ?>"><?php echo $value -> ten_the_loai ?></span>
                                                            </span>
                                                </a>
                                            </div>
                                        </div>
                                        <h1 class="title"><?php echo $value -> ten_bai_viet ?></h1>
                                        <!-- Post Meta  -->
                                        <div class="post-meta-wrapper">
                                            <div class="post-meta">
                                                <div class="post-author-avatar border-rounded">
                                                    <img src="public/image/user/<?php echo $value->anh_dai_dien; ?>" style="width: 40px; height:40px;" alt="Author Images">
                                                </div>
                                                <div class="content">
                                                    <h6 class="post-author-name">
                                                        <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $_REQUEST["id_bai_viet"] ?>">
                                                                    <span class="hover-flip-item">
                                                                        <span data-text="<?php echo $value -> ten ?>"><?php echo $value -> ten ?></span>
                                                                    </span>
                                                        </a>
                                                    </h6>
                                                    <ul class="post-meta-list">
                                                        <li><?php echo $value -> thoi_gian_tao ?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Post Content  -->
                                </div>
                                <!-- End Single Slide  -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Banner Area -->

                <div class="axil-post-details">
                    <div class="post-thumbnail">
                        <img src="public/image/post/<?php echo $value -> anh_tieu_de ?>">
                    </div>
                    <p class="has-medium-font-size"><?php echo $value -> noi_dung; }?></p>
                </div>
            </div>
        </div>

        <div class="axil-comment-area">
            <div class="axil-total-comment-post"></div>

            <!-- Start Comment Respond  -->
            <div class="comment-respond">
                <h4 class="title">Đăng bình luận</h4>
                <form method="POST" action="#">
                    <div class="col-12">
                        <div class="form-group">
                            <textarea name="binh_luan" id="comment-content"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-submit cerchio">
                            <button name="submit" type="button" id="submit-comment" class="axil-button button-rounded">
                                Đăng bình luận
                            </button>
                        </div>
                    </div>
            </div>
            </form>
        </div>
        <!-- End Comment Respond  -->

        <?php
        include("controllers/c_comment.php");
        $c_comment = new c_comment();
        $c_comment->index();
        ?>

    </div>
</div>



<!-- Start Comment Form Area  -->


<!--//post to api-->
<script>
    const comment = document.getElementById("comment-content")
    const reply_comment = document.getElementById('reply-comment')
    const formReplyComment = document.getElementById('reply-comment-form')
    let id_binh_luan = 0

    //submit comment
    document.getElementById("submit-comment").addEventListener('click', () => {
        $.post("comment.php",
            {
                noi_dung: comment.value,
                id_bai_viet: <?php echo $_GET['id_bai_viet']?>
            },
            (data, status) => {
                if(data === "You must logged in") {
                    toastErrorComment("Bạn phải đăng nhập để bình luận")
                    return
                }
                if (data !== 'success' || status !== 'success') {
                    return
                }
                toastSuccessComment("Đăng bình luận thành công")
                comment.value = ""
            });
    })

    //show reply comment form and get id_binh_luan
    const showReplyComment = (id_binh_luan) => {
        formReplyComment.classList.remove('reply-comment-form')
        reply_comment.focus()
        this.id_binh_luan = id_binh_luan
    };

    //submit reply comment
    document.getElementById("submit-reply-comment").addEventListener('click', () => {
        $.post("reply_comment.php",
            {
                noi_dung: reply_comment.value,
                id_binh_luan: this.id_binh_luan
            },
            (data, status) => {
                if(data === "You must logged in") {
                    toastErrorComment("Bạn phải đăng nhập để trả lời")
                    return
                }
                if (data !== 'success' || status !== 'success') {
                    return
                }
                toastSuccessComment("Trả lời thành công")
                formReplyComment.classList.add('reply-comment-form')
                reply_comment.value = ''
            });
    })

</script>
