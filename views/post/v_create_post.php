<div class="axil-post-list-area axil-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                    <!-- Start Contact Form  -->
                    <div>
                        <h2 class="title mb--10"><center>Đăng bài viết</center></h2>
                        <form
                                method="POST"
                                action="create_post.php"
                                id="form-post"
                                enctype="multipart/form-data"
                                class="contact-form--1 row">
                            <div class="col-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    <h6><b>Tiêu đề bài viết</b></h6>
                                    <input type="text" name="ten_bai_viet" id="tieu_de" placeholder="Tiêu đề..." required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Ảnh tiêu đề</label>
                                    <input type="file" name="anh_tieu_de" class="form-control"
                                           style="height: 35px; padding: 5px 10px; color: #3858F6;" required/>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Chọn địa điểm</label>
                                    <select id="select-the-loai" name="the_loai">
                                        <option value="1">Sapa</option>
                                        <option value="2">Đà Nẵng</option>
                                        <option value="3">Phú Quốc</option>
                                        <option value="4">Nha Trang</option>
                                    </select
                                </div>
                            </div>
                            <div class="col-12 reply-comment-form" id="image-1-area">
                                <div class="form-group">
                                    <label>Ảnh 1</label>
                                    <input type="file" id="image-1" name="anh_1" class="form-control"
                                           style="height: 35px; padding: 5px 10px; color: #3858F6;"/>
                                </div>
                            </div>
                            <div class="col-12 reply-comment-form" id="image-2-area">
                                <div class="form-group">
                                    <label>Ảnh 2</label>
                                    <input type="file" id="image-2" name="anh_2" class="form-control"
                                           style="height: 35px; padding: 5px 10px; color: #3858F6;"/>
                                </div>
                            </div>
                            <div class="col-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    <h6><b>Tag</b></h6>
                                        <ul class="social-with-text">
                                            <li class="youtube"><a><i class="fas fa-heading" id="btn-heading"></i><span>Tiêu đề</span></a></li>
                                            <li class="pinterest"><a><i class="fas fa-paragraph" id="btn-paragraph"></i><span>Đoạn văn</span></a></li>
                                            <li class="facebook"><a><i class="fas fa-bold" id="btn-bold"></i><span>In đậm</span></a></li>
                                            <li class="discord"><a><i class="fas fa-italic" id="btn-italic"></i><span>Chữ nghiêng</span></a></li>
                                            <li class="twitter"><a><i class="fas fa-link" id="btn-link"></i><span>Đường dẫn</span></a></li>
                                            <li class="instagram"><a><i class="fas fa-images" id="btn-image"></i><span>Ảnh</span></a></li>
                                        </ul>
                                </div>
                            </div>
                            <div class="col-12 reply-comment-form" id="link-area">
                                <div class="form-group">
                                    <h6><b>Đường dẫn</b></h6>
                                    <input type="text" id="link-href" placeholder="Đường dẫn..." style="width: 60%;">
                                    <input type="text" id="link-text" placeholder="Hiển thị..." style="width: 25%;">
                                    <button class="axil-button button-rounded" type="button" style="width: 10%;" id="link-btn-add">Điền</button>
                                </div>
                            </div>
                            <div class="col-12" style="margin-top: 10px;">
                                <div class="form-group">
                                    <h6><b>Nội dung</b></h6>
                                    <textarea required name="noi_dung" rows="8" placeholder="Nội dung..." id="post-content"></textarea>
                                </div>
                            </div>
                            <div class="col-12" style="display: flex">
                                <div class="form-submit">
                                    <input name="submit" id="btn-submit" value="Đăng bài viết" type="submit" class="axil-button button-rounded btn-primary" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Contact Form  -->
            </div>
        </div>
    </div>
</div>

<script>
    const heading = document.getElementById('btn-heading')
    const paragraph = document.getElementById('btn-paragraph')
    const bold = document.getElementById('btn-bold')
    const italic = document.getElementById('btn-italic')
    const link = document.getElementById('btn-link')
    const image = document.getElementById('btn-image')

    const postContent = document.getElementById('post-content')
    let currentPosition = postContent.selectionStart
    let isImage1Invisible = false

    heading.addEventListener('click', () => {
        //handle add heading
        let startPosition = postContent.selectionStart
        postContent.focus()

        if (startPosition != 0) {
            startPosition = startPosition + 5
        }

        currentPosition = startPosition + 4
        const text = '<h2></h2>'
        postContent.value = postContent.value + text
        postContent.setSelectionRange(currentPosition, currentPosition)
    })

    bold.addEventListener('click', () => {
        let startPosition = postContent.selectionStart
        postContent.focus()

        if (startPosition != 0) {
            startPosition = startPosition + 4
        }

        currentPosition = startPosition + 3
        const text = '<b></b>'
        postContent.value = postContent.value + text
        postContent.setSelectionRange(currentPosition, currentPosition)
    })

    italic.addEventListener('click', () => {
        let startPosition = postContent.selectionStart
        postContent.focus()

        if (startPosition != 0) {
            startPosition = startPosition + 4
        }

        currentPosition = startPosition + 3
        const text = '<i></i>'
        postContent.value = postContent.value + text
        postContent.setSelectionRange(currentPosition, currentPosition)
    })

    paragraph.addEventListener('click', () => {
        let startPosition = postContent.selectionStart
        postContent.focus()

        if (startPosition != 0) {
            startPosition = startPosition + 4
        }

        currentPosition = startPosition + 3
        const text = '<p></p>'
        postContent.value = postContent.value + text
        postContent.setSelectionRange(currentPosition, currentPosition)
    })

    link.addEventListener('click', () => {
        document.getElementById('link-area').classList.remove('reply-comment-form')
    })

    document.getElementById('link-btn-add').addEventListener('click', () => {
        const href = document.getElementById('link-href').value
        const content = document.getElementById('link-text').value

        document.getElementById('link-area').classList.add('reply-comment-form')

        let startPosition = postContent.selectionStart
        postContent.focus()

        if (startPosition != 0) {
            startPosition = startPosition + 4
        }

        currentPosition = startPosition + 15 + href.length + content.length
        const text = '<a href="' + href + '">' + content + '</a>'
        postContent.value = postContent.value + text
        postContent.setSelectionRange(currentPosition, currentPosition)
    })

    image.addEventListener('click', () => {
        if(isImage1Invisible) {
            document.getElementById('image-2-area').classList.remove('reply-comment-form')
            return
        }

        document.getElementById('image-1-area').classList.remove('reply-comment-form')
        isImage1Invisible = true
    })

    const removeFakePath = (path) => {
        const splits = path.split('fakepath\\')

        return splits[1]
    }

    document.getElementById('image-1').addEventListener('input', () => {
        const image1 = document.getElementById('image-1')

        postContent.focus()

        postContent.value = postContent.value +
            '<figure class="wp-block-image"> <img src="public/image/post/' + removeFakePath(image1.value) + '"> </figure>'

        currentPosition = currentPosition  + postContent.value.length
        postContent.setSelectionRange(currentPosition, currentPosition)
    })

    document.getElementById('image-2').addEventListener('input', () => {
        const image2 = document.getElementById('image-2')

        postContent.focus()

        postContent.value = postContent.value +
            '<figure class="wp-block-image"> <img src="public/image/post/' + removeFakePath(image2.value) + '"> </figure>'

        currentPosition = currentPosition  + postContent.value.length
        postContent.setSelectionRange(currentPosition, currentPosition)
    })

    // document.getElementById('btn-submit').addEventListener('click', () => {
    //     var form = $("#form-post");
    //
    //     // you can't pass Jquery form it has to be javascript form object
    //     var formData = new FormData(form[0]);
    //
    //     $.ajax({
    //         url: 'create_post.php',
    //         type: 'POST',
    //         data: formData,
    //         success: function (data) {
    //             console.log(data)
    //         },
    //         cache: false,
    //         contentType: false,
    //         processData: false
    //     });
    // })
</script>