<!-- Start Comment Area  -->
<div class="axil-comment-area">
    <ul class="comment-list" id="comment-list">
        <?php foreach ($comment as $key => $value) { ?>
            <!-- Start Single Comment  -->
            <li class="comment">
                <div class="comment-body">
                    <div class="single-comment">
                        <div class="comment-img">
                            <img src="public/image/user/<?php echo $value->anh_dai_dien; ?>"
                                 style="width: 50px; height: 50px;">
                        </div>
                        <div class="comment-inner">
                            <h6 class="commenter">
                                <a class="hover-flip-item-wrapper">
                                    <span class="hover-flip-item">
                                        <span data-text="<?php echo $value->ten; ?>"><?php echo $value->ten; ?></span>
                                    </span>
                                </a>
                            </h6>
                            <div class="comment-meta">
                                <div class="time-spent"><?php echo $value->thoi_gian; ?></div>
                                <div class="reply-edit">
                                    <div class="reply">
                                        <a class="comment-reply-link hover-flip-item-wrapper">
                                            <span class="hover-flip-item">
                                                <span data-text="Trả lời" class="tra_loi" onclick="showReplyComment(<?php echo $value->id; ?>)">Trả lời</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-text">
                                <p class="b2"><?php echo $value->noi_dung; ?> </p>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="children" id="comment-<?php echo $value->id; ?>">
                <?php
                    include_once("controllers/c_reply_comment.php");

                    $c_reply_comment = new c_reply_comment();
                    $c_reply_comment->index($value->id);
                ?>
                </ul>
            </li>
            <!-- End Single Comment  -->
        <?php } ?>

    </ul>
</div>
<!-- End Comment Area  -->

<!-- Start Comment Form Area  -->
<div class="axil-comment-area">
    <!-- Start Comment Respond  -->
    <div class="comment-respond reply-comment-form" id="reply-comment-form">
        <form style="display: flex;">
            <div class="col-8">
                <div class="form-group">
                    <input name="tra_loi_binh_luan" id="reply-comment" type="text"
                           placeholder="Trả lời bình luận"/>
                </div>
            </div>
            <div class="col-8" style="margin-left: 10px;">
                <div class="form-submit cerchio">
                    <button type="button" id="submit-reply-comment" class="axil-button button-rounded">
                        Trả lời
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Comment Respond  -->

</div>
<!-- End Comment Form Area  -->

