
<h1 class="d-none">Home Tech Blog</h1>
<!-- Start Post List Wrapper  -->
<div class="axil-post-list-area axil-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-8">
                <!-- Start Post List  -->
                <h2> Danh mục <?php echo $ten_the_loai ?></h2>
                <?php foreach ($post_list as $key => $value) { ?>
                    <!--Cứ save ảnh vào public/layout/assets/images/post-images/ nhé -->
                    <div class="content-block post-list-view format-quote mt--30">
                        <div class="post-thumbnail">
                            <a href="post_detail.php?id_bai_viet=<?php echo $value->id ?>">
                                <img src="public/image/post/<?php echo $value->anh_tieu_de?>" alt="Post Images">
                            </a>
                        </div>

                        <div class="post-content">
                            <div class="post-cat">
                                <div class="post-cat-list">
                                    <a class="hover-flip-item-wrapper" href="#">
                                            <span class="hover-flip-item">
                                                <span data-text="<?php echo $value -> ten_the_loai;?>"><?php echo $value -> ten_the_loai;?></span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                            <blockquote>
                                <h4 class="title"><a href="post_detail.php?id_bai_viet=<?php echo $value->id ?>"><?php echo $value -> ten_bai_viet;?></a></h4>
                            </blockquote>
                            <div class="post-meta-wrapper">
                                <div class="post-meta">
                                    <div class="content">
                                        <h6 class="post-author-name">
                                            <a class="hover-flip-item-wrapper" href="author.php?id_bai_viet=<?php echo $value->id ?>">
                                                    <span class="hover-flip-item">
                                                        <span data-text="<?php echo $value->ten?>"><?php echo $value->ten?></span>
                                                    </span>
                                            </a>
                                        </h6>
                                        <ul class="post-meta-list">
                                            <li><?php echo $value -> thoi_gian_tao;?></li>
                                            <li><?php   $date = new DateTime($value -> thoi_gian_tao);
                                                $now = new DateTime();
                                                echo $date->diff($now)->format("%d ngày, %h giờ, %i phút"); ?></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--                            <ul class="social-share-transparent justify-content-end">-->
                                <!--                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>-->
                                <!--                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>-->
                                <!--                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>-->
                                <!--                                <li><a href="#"><i class="fas fa-link"></i></a></li>-->
                                <!--                            </ul>-->
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- End Post List  -->


            </div>


            <?php
            include_once ('views/side_bar/v_side_bar.php')
            ?>
        </div>
    </div>
</div>
<!-- End Post List Wrapper  -->