<?php foreach ($reply_comment as $key => $value) { ?>
        <li class="comment">
            <div class="comment-body">
                <div class="single-comment">
                    <div class="comment-img">
                        <img src="public/image/user/<?php echo $value -> anh_dai_dien; ?>"
                             alt="Author Images" style="width: 50px; height: 50px;">
                    </div>
                    <div class="comment-inner">
                        <h6 class="commenter">
                            <a class="hover-flip-item-wrapper" href="#">
                                <span class="hover-flip-item">
                                    <span data-text="<?php echo $value -> ten; ?>"><?php echo $value -> ten; ?></span>
                                </span>
                            </a>
                        </h6>
                        <div class="comment-meta">
                            <div class="time-spent"><?php echo $value -> thoi_gian; ?></div>
                        </div>
                        <div class="comment-text">
                            <p class="b2"><?php echo $value -> noi_dung; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </li>
<?php } ?>