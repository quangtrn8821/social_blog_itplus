<h1 class="d-none">Home Tech Blog</h1>
<!-- Start Post List Wrapper  -->
<div class="axil-post-list-area axil-section-gap bg-color-white">
    <div class="container">
        <div class="row" id="noti-area">
            <?php foreach ($notification as $key => $value) { ?>
                <div class="col-lg-8 col-xl-8 mt--10">
                    <div class="post-content post-notification" style="">
                        <div class="post-cat">
                            <div class="post-cat-list">
                                <a class="hover-flip-item-wrapper">
                                    <span class="hover-flip-item">
                                            <span data-text="<?php echo $value->ten ?>"><?php echo $value->ten ?></span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <span class="title">
                            <a href="post_detail.php?id_bai_viet=<?php echo $value->id_bai_viet ?>">
                            <?php echo $value->noi_dung ?>
                            </a>
                        </span> <!--Content-->
                        <h6 style="margin: 0; color: #888"><?php echo $value->thoi_gian ?></h6> <!--Time-->
                    </div>
                </div>
            <?php } ?>


            <?php
            include_once('views/side_bar/v_side_bar.php')
            ?>
        </div>
    </div>
</div>
<!-- End Post List Wrapper  -->
