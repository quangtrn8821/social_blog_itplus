<?php
session_start();
include "controllers/c_user.php";
$c_user = new c_user();
if (isset($_SESSION["user"])) {
    $c_user->get_user();
} else {
    header("location: sign_in.php");
}
?>