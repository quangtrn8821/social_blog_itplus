<?php
session_start();
include "controllers/c_post_list.php";
include "controllers/c_search.php";

$c_post_list = new c_post_list();

if(!empty($_POST['search-btn'])) {
   $c_search = new c_search();
   $c_search -> search();
} else {
    $c_post_list -> index();
}
?>