<?php
include_once ("models/database/database.php");

class m_reply_comment extends database {
    public function get_reply_comment($id_binh_luan) {
        $sql = "SELECT tra_loi_binh_luan.id, tra_loi_binh_luan.noi_dung,tra_loi_binh_luan.thoi_gian, tra_loi_binh_luan.id_binh_luan, nguoi_dung.ten, nguoi_dung.anh_dai_dien 
                FROM tra_loi_binh_luan INNER JOIN nguoi_dung 
                ON tra_loi_binh_luan.id_nguoi_dung = nguoi_dung.id 
                WHERE tra_loi_binh_luan.id_binh_luan = $id_binh_luan AND tra_loi_binh_luan.trang_thai = 1";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function add_reply_comment($noi_dung, $id_nguoi_dung, $id_binh_luan) {
        $sql = "INSERT INTO tra_loi_binh_luan (noi_dung, id_nguoi_dung, id_binh_luan) VALUES (?, ?, ?);";
        $this -> setQuery($sql);
        return $this->execute(array($noi_dung, $id_nguoi_dung, $id_binh_luan));
    }
}

?>