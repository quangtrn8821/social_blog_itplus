<?php
include_once ("models/database/database.php");

class m_comment extends database {
    public function get_comment($id_bai_viet) {
        $sql = "SELECT binh_luan.id, binh_luan.id_nguoi_dung,binh_luan.id_bai_viet, binh_luan.noi_dung, nguoi_dung.ten, binh_luan.thoi_gian, nguoi_dung.anh_dai_dien 
                FROM binh_luan INNER JOIN nguoi_dung ON binh_luan.id_nguoi_dung = nguoi_dung.id 
                WHERE id_bai_viet = $id_bai_viet AND trang_thai = 1;";
        $this -> setQuery($sql);
        return $this->loadAllRows();
    }

    public function add_comment($noi_dung, $id_bai_viet, $id_nguoi_dung) {
        $sql = "INSERT INTO binh_luan (noi_dung, id_nguoi_dung, id_bai_viet) VALUES (?, ?, ?);";
        $this -> setQuery($sql);
        return $this->execute(array($noi_dung, $id_nguoi_dung, $id_bai_viet));
    }

    public function get_id_by_id_comment($id_comment) {
        $sql = "SELECT id_nguoi_dung, id_bai_viet FROM binh_luan WHERE id=?";
        $this -> setQuery($sql);
        return $this->loadAllRows(array($id_comment));
    }

    public function get_last_comment_id() {
        $sql = "SELECT MAX(LAST_INSERT_ID(id)) FROM binh_luan";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
}
?>