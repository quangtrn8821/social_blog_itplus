<?php
include_once("models/database/database.php");

class m_user extends database
{
    public function read_user_by_email($email)
    {
        $sql = "select * from nguoi_dung where email = ? ";
        $this->setQuery($sql);
        return $this->loadAllRows(array($email));
    }

    public function update_user($id, $ten, $email, $sdt, $mo_ta, $anh_dai_dien)
    {
        $sql = "update nguoi_dung set ten=?, email=?, sdt=?, mo_ta=?, anh_dai_dien=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten, $email, $sdt, $mo_ta, $anh_dai_dien, $id));
    }

    public function read_user_by_id($id) {
        $sql = "select * from nguoi_dung where id = ? ";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
}

?>