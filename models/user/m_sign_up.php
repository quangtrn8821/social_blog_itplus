<?php
include_once("models/database/database.php");

class m_sign_up extends database
{
    public function add_user($ten, $email, $sdt, $mat_khau)
    {
        $sql = "insert into nguoi_dung (ten, email, sdt, mat_khau) values(?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($ten, $email, $sdt, $mat_khau));
    }

    public function return_user_by_email($email)
    {
        $sql = "SELECT COUNT(*) as KQ FROM nguoi_dung WHERE email = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($email));
    }
}

?>