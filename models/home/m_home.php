<?php
require_once "models/database/database.php";
class m_home extends database {
    public function showPostTechnology() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 1 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPostTechnology2() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 1 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 2 OFFSET 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPostCulinary() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 3 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPostCulinary2() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 3 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 2 OFFSET 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPostTravel() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 4 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPostTravel2() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 4 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 3 OFFSET 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPostFashion() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 2 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPostFashion2() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 2 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 2 OFFSET 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPostFashion3() {

        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` FROM `bai_viet`,`the_loai`,`nguoi_dung` WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND `bai_viet`.`id_the_loai` = 2 AND  `bai_viet`.`trang_thai`=1 ORDER BY `bai_viet`.`id` DESC LIMIT 2 OFFSET 3";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
}
?>