<?php
include_once("models/database/database.php");

class m_search extends database
{

    public function get_search_data($ten_bai_viet)
    {
        if (!isset($_REQUEST["page"])) {
            $page = 1;
        } else {
            $page = $_REQUEST["page"];
        }
        $page_result = ($page - 1) * 6;
        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` 
                FROM `bai_viet`
                LEFT JOIN  `nguoi_dung` 
                ON `bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id`
                LEFT JOIN `the_loai`
                ON  `bai_viet`.`id_the_loai`=`the_loai`.`id` 
                WHERE `bai_viet`.`trang_thai`=1 AND  `bai_viet`.`ten_bai_viet` LIKE '%$ten_bai_viet%'
                ORDER BY bai_viet.thoi_gian_tao DESC";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function showPageNumber($ten_bai_viet)
    {
        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` 
                FROM `bai_viet`
                LEFT JOIN  `nguoi_dung` 
                ON `bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id`
                LEFT JOIN `the_loai`
                ON  `bai_viet`.`id_the_loai`=`the_loai`.`id` 
                WHERE `bai_viet`.`trang_thai`=1 AND  `bai_viet`.`ten_bai_viet` LIKE '%$ten_bai_viet%'
                ORDER BY bai_viet.thoi_gian_tao DESC ";

        $this->setQuery($sql);
        $post_list = $this->loadAllRows();
        $page_number = count($post_list);
        $number_of_page = ceil($page_number / $page_number);

        return $number_of_page;
    }
}