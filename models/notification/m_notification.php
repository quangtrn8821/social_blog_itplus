<?php
include_once ("models/database/database.php");

class m_notification extends database {
    public function add_notification($noi_dung, $id_bai_viet, $id_nguoi_comment, $id_tac_gia) {
        $sql = "INSERT INTO thong_bao (noi_dung, id_bai_viet, id_nguoi_comment, id_tac_gia) VALUES (?, ?, ?, ?);";
        $this -> setQuery($sql);
        return $this->execute(array($noi_dung, $id_bai_viet, $id_nguoi_comment, $id_tac_gia));
    }

    public function get_notification($id_nguoi_dung) {
        $sql = "SELECT thong_bao.id, thong_bao.id_bai_viet, nguoi_dung.ten, thong_bao.noi_dung, thong_bao.thoi_gian
                FROM thong_bao INNER JOIN nguoi_dung ON thong_bao.id_nguoi_comment = nguoi_dung.id
                WHERE thong_bao.id_tac_gia=? ORDER BY thong_bao.id DESC LIMIT 5";
        $this -> setQuery($sql);
        return $this->loadAllRows(array($id_nguoi_dung));
    }
}
?>