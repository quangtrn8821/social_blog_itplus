<?php
require_once ("models/database/database.php");

class m_create_post extends database {
    public function create_post($ten_bai_viet, $noi_dung, $id_the_loai, $anh_tieu_de, $id_nguoi_dung, $thoi_gian_tao) {
        $sql = 'INSERT INTO bai_viet (ten_bai_viet, noi_dung, id_the_loai, anh_tieu_de, id_nguoi_dung, thoi_gian_tao) VALUES (?,?,?,?,?,?)';
        $this->setQuery($sql);
        return $this->execute(array($ten_bai_viet, $noi_dung, $id_the_loai, $anh_tieu_de, $id_nguoi_dung, $thoi_gian_tao));
    }
}
?>