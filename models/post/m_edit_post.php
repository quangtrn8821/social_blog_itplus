<?php
require_once ("models/database/database.php");

class m_edit_post extends database {

    public function get_post_by_id_bai_viet($id_bai_viet) {
        $sql = "SELECT * FROM bai_viet WHERE trang_thai = 1 AND id=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_bai_viet));
    }

    public function update_post($ten_bai_viet, $noi_dung, $id_bai_viet) {
        $sql = 'UPDATE bai_viet SET ten_bai_viet=?, noi_dung=? WHERE id=?';
        $this->setQuery($sql);
        return $this->execute(array($ten_bai_viet, $noi_dung, $id_bai_viet));
    }
}
?>