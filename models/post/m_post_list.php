<?php
require_once "models/database/database.php";

class m_post_list extends database {
    public function showPostList() {
        if(!isset( $_REQUEST["page"]))
        {
            $page=1;
        }
        else
        {
            $page = $_REQUEST["page"];
        }
        $page_result= ($page - 1) * 6;
        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` 
                FROM `bai_viet`,`the_loai`,`nguoi_dung` 
                WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND  `bai_viet`.`trang_thai`=1 
                ORDER BY bai_viet.thoi_gian_tao DESC LIMIT ".$page_result.", 6";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }
    public function showPageNumber()
    {
        $sql = "SELECT `bai_viet`.`id`, `bai_viet`.`ten_bai_viet`, `bai_viet`.`noi_dung`, `bai_viet`.`thoi_gian_tao`,`bai_viet`. `id_the_loai`, `bai_viet`.`trang_thai`, `bai_viet`.`anh_tieu_de`, `bai_viet`.`id_nguoi_dung`,`the_loai`.`ten_the_loai`,`nguoi_dung`.`ten` 
                FROM `bai_viet`,`the_loai`,`nguoi_dung` 
                WHERE `bai_viet`.`id_the_loai`=`the_loai`.`id` AND`bai_viet`.`id_nguoi_dung`=`nguoi_dung`.`id` AND  `bai_viet`.`trang_thai`=1 
                ORDER BY bai_viet.thoi_gian_tao DESC";

        $this->setQuery($sql);
        $post_list = $this->loadAllRows();
        $page_number = count($post_list);
        $number_of_page = ceil($page_number/ 6 );

        return $number_of_page;
    }

    public function read_user_post_list($email_user) {
        $sql = "SELECT bai_viet.id, bai_viet.ten_bai_viet, the_loai.ten_the_loai, nguoi_dung.ten, bai_viet.thoi_gian_tao, nguoi_dung.email, bai_viet.anh_tieu_de
                FROM nguoi_dung INNER JOIN bai_viet ON nguoi_dung.id = bai_viet.id_nguoi_dung 
                INNER JOIN the_loai ON bai_viet.id_the_loai = the_loai.id
                WHERE bai_viet.trang_thai = 1 AND nguoi_dung.email = ? ";
        $this -> setQuery($sql);
        return $this -> loadAllRows(array($email_user));
    }

    public function read_all_post_list() {
        $sql = "SELECT bai_viet.id, bai_viet.ten_bai_viet, the_loai.ten_the_loai, nguoi_dung.ten, bai_viet.thoi_gian_tao, nguoi_dung.email, bai_viet.anh_tieu_de
                FROM nguoi_dung INNER JOIN bai_viet ON nguoi_dung.id = bai_viet.id_nguoi_dung 
                INNER JOIN the_loai ON bai_viet.id_the_loai = the_loai.id
                WHERE bai_viet.trang_thai = 1";
        $this -> setQuery($sql);
        return $this -> loadAllRows();
    }

    public function read_user_by_id_post($id_bai_viet) {
        $sql = "SELECT nguoi_dung.id, nguoi_dung.ten, nguoi_dung.email, nguoi_dung.mo_ta, nguoi_dung.phan_quyen, nguoi_dung.anh_dai_dien
                FROM nguoi_dung INNER JOIN bai_viet ON nguoi_dung.id = bai_viet.id_nguoi_dung 
                WHERE bai_viet.trang_thai = 1 AND bai_viet.id = ?";
        $this -> setQuery($sql);
        return $this -> loadAllRows(array($id_bai_viet));
    }

    public function delete_post($id_bai_viet) {
        $sql = "update bai_viet set trang_thai = 0 where id=?";
        $this->setQuery($sql);
        return $this->execute(array($id_bai_viet));
    }
}
?>